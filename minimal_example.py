from openfisca_france_data import france_data_tax_benefit_system
from openfisca_france_data.erfs_fpr.get_survey_scenario import get_survey_scenario
import sys

"""
Dans la console, en adaptant le chemin d'accès':
cd /Users/pablorodriguez/openfisca-survey-manager/openfisca_survey_manager/scripts 
python build_collection.py -c erfs_fpr -d -m -s 2018
"""
sys.argv = ['-y 2018']

# En adaptant le chemin d'accès :
exec(open("/Users/pablorodriguez/openfisca-france-data/openfisca_france_data/erfs_fpr/input_data_builder/__init__.py").read())

tax_benefit_system = france_data_tax_benefit_system

survey_scenario = get_survey_scenario(
    tax_benefit_system = tax_benefit_system,
    year = 2018,
    rebuild_input_data = False
    )

survey_scenario.compute_aggregate(variable = 'irpp', period = 2018)


"""
Ce test est le test standard que j'ai lancé une fois la première installation faite.
Parmi les différents essais que j'ai réalisé, il semble essentiel de 
faire python build_collection.py -c erfs_fpr -d -m -s [year], sans quoi le
test ne marchera pas. Ensuite, aussi bien le rebuild_input_data=True que le 
rebuild_input_data=False permettent de lancer le script sans problèmes.
Ce qui est bizarre est le fait que le script s'exécute bien pour l'année 2014 
mais non pas pour l'année 2018. 
"""
