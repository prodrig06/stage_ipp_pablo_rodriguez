# -*- coding: utf-8 -*-
"""

@author: Pablo Rodriguez

Code computes marginal tax rates for test cases
1) Reforms coding (allows to mute some taxes/benefits), apply them to two TBS
2) Plot results over income range
3) Plot rates only

"""

################
# CONSOLE
################

case_filename = "norsa"
household_composition = "adult1_child0"
year = 2015
social_contributions_muted = True
varying = "salaire_de_base" # can be "salaire_de_base" or "salaire_imposable" 
x = 3 ## upper bound of graphs and tables in terms of SMIC

#############################
# Remarks on this code

# Salaire_imposable is probably not what we want because progressivity could (in theory) 
# take place among contributions and social "cotisations"
# 

#############################
case_titles = {"everything": " ", "norsa": "No RSA", "norsa_noppe": "No RSA, no PPE", 
               "norsa_noppe_nodecote": "No RSA, no PPE, no Decote"}
sc_muting_note = {False : "sc", True: "nosc"}
path_varying = {"salaire_de_base": "varying_salaire_de_base_", 
                "salaire_imposable": "varying_salaire_imposable_",
                "rni": "varying_rni_", # this is likely to be temporary, it is used as a check
                }
full_names_for_varying = {"salaire_de_base": "Revenu brut du travail",
                          "salaire_imposable": "Revenu imposable du travail"}
name_varying = full_names_for_varying[varying]
household_compositions = {"adult1_child0": {},
                          "adult1_child0_pub": {'parent1_age': 30, 'categorie_salarie': 'public_titulaire_etat'},# does not work
                          "adult1_child1": {'nb_enfants' : 1, 'enfants_age' : [5]},
                          "adult1_child2": {'nb_enfants' : 2, 'enfants_age' : [5, 6]},
                          "adult1_child3": {'nb_enfants' : 3, 'enfants_age' : [5, 6, 7]},
                          "adult2_child0" : {'parent2_age': 40, 'couple' : True, 'union_legale':True},
                          "adult2_child1" : {'parent2_age': 40, 'couple' : True, 'union_legale':True, 
                                             'nb_enfants' : 1, 'enfants_age' : [5]} }

import copy

# For our local version
from openfisca_france_data import france_data_tax_benefit_system
from openfisca_core import periods

# For reform
#from numpy import logical_and as and_, maximum as max_
from openfisca_france.model.base import *
from numpy import around
from openfisca_core.reforms import Reform

# Output path
import os
current_path = str(os.getcwd())
graphs_path = (os.path.join(current_path, "graphs/"))
data_output_path = (os.path.join(current_path, "data_output_test_cases/"))


#import time
#time.sleep(0)
# For figures
#import matplotlib.pyplot as plt


# TBS initialization
##########################        

tax_benefit_system = france_data_tax_benefit_system

tbs_copy = copy.deepcopy(tax_benefit_system)
tbs_copy.entities = tax_benefit_system.entities
        
# BEWARE Package below must be imported AFTER deep copy of the TBS
from taxipp.utils.test_case.test_case_utils import plot_test_case, plot_tax_rates

######################
class mute_decote(Reform):
    name = "Mute the decote mechanism in the computation of net taxes"
    def apply(self):
        class ip_net(Variable):
            value_type = float
            entity = FoyerFiscal
            label = "Impôt sur le revenu après décote et réduction sous condition de revenus, avant réductions"
            definition_period = YEAR
            def formula(foyer_fiscal, period, parameters):
                '''
                Impôt net avant réductions
                '''
                cncn_info_i = foyer_fiscal.members('cncn_info', period)
                decote = foyer_fiscal('decote', period)
                ir_plaf_qf = foyer_fiscal('ir_plaf_qf', period)
                taux = parameters(period).impot_revenu.rpns.taux16
                # N'est pas véritablement une 'réduction', cf. la définition de cette variable
                reduction_ss_condition_revenus = foyer_fiscal('reduction_ss_condition_revenus', period)
                # decote and reduction_ss_condition_revenus replaced by 0 below (they are complementary)
                return around(max_(0, ir_plaf_qf + foyer_fiscal.sum(cncn_info_i) * taux - 0 - 0))
        class decote(Variable):
            value_type = float
            entity = FoyerFiscal
            label = "Decote set to 0"
            definition_period = YEAR
            def formula_2014_01_01(foyer_fiscal, period, parameters):
                return 0
            def formula_2001_01_01(foyer_fiscal, period, parameters):
                return 0
        class decote_gain_fiscal(Variable):
            value_type = float
            entity = FoyerFiscal
            label = "Decote gain fiscal set to 0"
            definition_period = YEAR
            def formula(foyer_fiscal, period, parameters):
                return 0
        self.replace_variable(ip_net)
        self.replace_variable(decote_gain_fiscal)
        self.replace_variable(decote)
        # nullyfying the decote variable should be enough
######################
class mute_ppe(Reform):
    name = "Mute the ppe"
    def apply(self):
        class ppe_brute(Variable):
            value_type = float
            entity = FoyerFiscal
            label = "Muted PPE"
            definition_period = YEAR
            def formula(foyer_fiscal, period, parameters):
                '''
                PPE brute (fixee a 0)
                '''
                return around(0)
        self.replace_variable(ppe_brute)
######################
class mute_rsa(Reform):
    name = "Mute the RSA"
    def apply(self):
        class rsa(Variable):
            value_type = float
            entity = Famille
            label = "Muted RSA"
            definition_period = MONTH
            def formula_2009_06_01(famille, period, parameters):
                '''
                RSA muted (set to 0)
                '''
                return 0
        self.replace_variable(rsa)
######################
class mute_reduc_exceptionelle(Reform):
    name = "Mute the reduction impot exceptionnelle Valls"
    def apply(self):
        class reduction_impot_exceptionnelle(Variable):
            value_type = float
            entity = FoyerFiscal
            label = "Muted reduction exceptionnelle"
            end = '2013-12-31'
            definition_period = YEAR
            def formula_2013_01_01(foyer_fiscal, period, parameters):
                '''
                Muted (set to 0)
                '''
                return 0
        self.replace_variable(reduction_impot_exceptionnelle)

######################
class mute_aides_logement(Reform):
    name = "Mute housing benefits"
    def apply(self):
        class aide_logement(Variable):
            value_type = float
            entity = Famille
            label = "Aide au logement tout type"
            definition_period = MONTH
            def formula(famille, period):
                '''
                Aide au logement
                '''
                apl = famille('apl', period)
                als = famille('als', period)
                alf = famille('alf', period)
                return 0
                #return max_(max_(apl, als), alf)
        self.replace_variable(aide_logement)
######################
class mute_social_contributions(Reform):
    name = "Remove CSG and CRDS"
    def apply(self):
        class csg_imposable_salaire(Variable):
            value_type = float
            entity = Individu
            label = "CSG imposable employee"
            definition_period = MONTH
            def formula(individu, period, parameters):
                '''
                muted formula
                '''
                return 0
        class csg_deductible_salaire(Variable):
            value_type = float
            entity = Individu
            label = "CSG deductible employee"
            definition_period = MONTH
            def formula(individu, period, parameters):
                '''
                muted formula
                '''
                return 0
        class crds_salaire(Variable):
            value_type = float
            entity = Individu
            label = "CRDS imposable employee"
            definition_period = MONTH
            def formula(individu, period, parameters):
                '''
                muted formula
                '''
                return 0
        class cotisations_salariales(Variable):
            value_type = float
            entity = Individu
            label = "social contributions on employees"
            definition_period = MONTH
            def formula(individu, period, parameters):
                '''
                muted formula
                '''
                return 0
            
        self.replace_variable(csg_imposable_salaire)
        self.replace_variable(csg_deductible_salaire)
        self.replace_variable(crds_salaire)
        self.replace_variable(cotisations_salariales)
######################
class create_agg_social_contributions(Reform):
    name = "Aggregate of all social contributions"
    def apply(self):
        class agg_social_contributions(Variable):
            value_type = float
            entity = Individu
            label = "Sum of CSG, CRDS and other social contributions"
            definition_period = MONTH
            def formula(individu, period, parameters):
                '''
                Simple sum
                '''
                cotisations_salariales = individu('cotisations_salariales', period)
                crds_salaire = individu('crds_salaire', period)
                csg_imposable_salaire = individu('csg_imposable_salaire', period)
                csg_deductible_salaire = individu('csg_deductible_salaire', period)                
                return cotisations_salariales + crds_salaire + csg_imposable_salaire + csg_deductible_salaire
        self.add_variable(agg_social_contributions)

######################
def null_decote(parameters):
    year = 2014
    reform_period = periods.period(str(year))
    # Reform
    parameters.impot_revenu.decote.taux.update(start = reform_period, value = 0)
    parameters.impot_revenu.decote.seuil_celib.update(start = reform_period, value = 0 )
    parameters.impot_revenu.decote.seuil_couple.update(start = reform_period, value = 0 )
    return parameters

class mute_decote_by_parameters(Reform):
    name = "explicit_title"
    def apply(self):
        self.modify_parameters(modifier_function = null_decote)
#######################

### Modifications for both systems: mute housing benefits and add one variable
# System without reform
tax_benefit_system = mute_aides_logement(tax_benefit_system)
#tax_benefit_system = create_agg_social_contributions(tax_benefit_system)
# System with reform
tax_benefit_system_reforme = mute_aides_logement(tbs_copy)
#tax_benefit_system_reforme = create_agg_social_contributions(tax_benefit_system_reforme)


if social_contributions_muted == True:
    ### System without reform
    tax_benefit_system = mute_social_contributions(tax_benefit_system)
    # System with reform
    tax_benefit_system_reforme = mute_social_contributions(tax_benefit_system_reforme)


### Reforms used for isolating the effect of specific instruments: modify only the reform TBS
if case_filename == "norsa":
    tax_benefit_system_reforme = mute_rsa(tax_benefit_system_reforme)
if case_filename == "norsa_noppe":
    tax_benefit_system_reforme = mute_ppe(tax_benefit_system_reforme)
    tax_benefit_system_reforme = mute_rsa(tax_benefit_system_reforme)
if case_filename == "norsa_noppe_nodecote":
    tax_benefit_system_reforme = mute_ppe(tax_benefit_system_reforme)
    tax_benefit_system_reforme = mute_rsa(tax_benefit_system_reforme)
    tax_benefit_system_reforme = mute_decote(tax_benefit_system_reforme)
    #tax_benefit_system_reforme = mute_decote_by_parameters(tbs_copy)
if case_filename == "norsa_noppe_noreduc":
    tax_benefit_system_reforme = mute_ppe(tax_benefit_system_reforme)
    tax_benefit_system_reforme = mute_rsa(tax_benefit_system_reforme)
    tax_benefit_system_reforme = mute_reduc_exceptionelle(tax_benefit_system_reforme)


##########################
# Plot tax levels
##########################
decomposition_list = ['rsa', 'csg_imposable_salaire', 'irpp', 'ppe', 'rni',
                      'csg_deductible_salaire', 'decote_gain_fiscal'] 
decomposition_list = ['rsa', 'rsa_activite', 'irpp','ppe','rni',
                      'decote_gain_fiscal']            
# Note: graphs make no sense with that list because elements are not exclusive
# But we want to have absolute values for all of them          
reform_df, baseline_df, reform_figure, baseline_figure = plot_test_case(
    baseline_tax_benefit_system = tax_benefit_system,
    tax_benefit_system = tax_benefit_system_reforme, #tax_benefit_system_reforme,
    period = str(year),
    return_dataframes = True,
    input_variable = varying,
    graph_options = {
        'x_variable': varying,
        'y_variable': 'revenu_disponible',
        'decomposition_reference': decomposition_list ,
        'decomposition_reform': decomposition_list ,
        'nb_smic_max': x,
        'count' : 300, ## 300 estimation points (default is 100)
        },
    test_case_options = household_compositions[household_composition],
    monthly = False,
    )

print("Proportion of simulated population receiving RSA: ",
      len(baseline_df[ baseline_df['Revenu de Solidarité Active'] != 0 ])/len(baseline_df))


#### Clean save



baseline_df.to_csv(data_output_path + path_varying[varying] + "taxlevels_"+household_composition+"_"+str(year)+
                     "_"+case_filename+"_"+
                        sc_muting_note[social_contributions_muted]+".csv", sep = ';')
reform_df.to_csv(data_output_path + path_varying[varying] + "taxlevels_ref_"+household_composition+"_"+str(year)+
                     "_"+case_filename+"_"+
                        sc_muting_note[social_contributions_muted]+
                        ".csv", sep = ';')
note = sc_muting_note[social_contributions_muted]
print(note)
baseline_figure.savefig(graphs_path + path_varying[varying] + "taxlevels_"+str(household_composition)+
                        "_"+str(year)+"_"+case_filename+"_"+ note + ".png")
reform_figure.savefig(graphs_path+path_varying[varying] + "taxlevels_"+str(household_composition)+
                      "_"+str(year)+"_"+case_filename+"_"+ sc_muting_note[social_contributions_muted]+"_ref.png")


# Baseline
rsa_fig = baseline_df.plot(name_varying, 'Revenu de Solidarité Active').get_figure()
#rsa_fig.savefig(graphs_path+path_varying[varying] + "levels_rsa_"+str(household_composition)+
#                      "_"+str(year)+"_"+case_filename+"_"+ sc_muting_note[social_contributions_muted]+".png")

rsa_act_fig = baseline_df.plot(name_varying, 'rsa_activite').get_figure()
#rsa_act_fig.savefig(graphs_path+path_varying[varying] + "levels_rsa_"+str(household_composition)+
#                      "_"+str(year)+"_"+case_filename+"_"+ sc_muting_note[social_contributions_muted]+".png")

ppe_fig = baseline_df.plot(name_varying, 'PPE').get_figure()
#ppe_fig.savefig(graphs_path+path_varying[varying] + "levels_ppe_"+str(household_composition)+
#                      "_"+str(year)+"_"+case_filename+"_"+ sc_muting_note[social_contributions_muted]+".png")
decote_fig = baseline_df.plot(name_varying, 'decote_gain_fiscal').get_figure()
#decote_fig.savefig(graphs_path+path_varying[varying] + "levels_decote_"+str(household_composition)+
#                      "_"+str(year)+"_"+case_filename+"_"+ sc_muting_note[social_contributions_muted]+".png")

# With reform
rsa_fig = reform_df.plot(name_varying, 'Revenu de Solidarité Active').get_figure()
#rsa_fig.savefig(graphs_path+path_varying[varying] + "levels_rsa_"+str(household_composition)+
#                      "_"+str(year)+"_"+case_filename+"_"+ sc_muting_note[social_contributions_muted]+"_ref.png")

rsa_act_fig = baseline_df.plot(name_varying, 'rsa_activite').get_figure()
#rsa_act_fig.savefig(graphs_path+path_varying[varying] + "levels_rsa_"+str(household_composition)+
#                      "_"+str(year)+"_"+case_filename+"_"+ sc_muting_note[social_contributions_muted]+".png")

ppe_fig = reform_df.plot(name_varying, 'PPE').get_figure()
#ppe_fig.savefig(graphs_path+path_varying[varying] + "levels_ppe_"+str(household_composition)+
#                      "_"+str(year)+"_"+case_filename+"_"+ sc_muting_note[social_contributions_muted]+"_ref.png")
decote_fig = reform_df.plot(name_varying, 'decote_gain_fiscal').get_figure()
#decote_fig.savefig(graphs_path+path_varying[varying] + "levels_decote_"+str(household_composition)+
#                      "_"+str(year)+"_"+case_filename+"_"+ sc_muting_note[social_contributions_muted]+"_ref.png")

#plt.plot(reform_df["Revenu brut du travail"], reform_df["irpp"])
#plt.plot(reform_df["Revenu brut du travail"], reform_df["decote_gain_fiscal"])
#plt.plot(reform_df["Revenu brut du travail"], reform_df["ir_plaf_qf"])
#plt.plot(reform_df["irpp"], reform_df["ir_plaf_qf"])

# Using recourant_RSA = False has no impact

# Notes from documentation (manual exploration of codes)
########################################################
#if (input_variable == "salaire_net" or input_variable == "salaire_de_base") 
# code uses an actif
# else:
# scenario created is for a "rentier"
#    graph_options : {x_variable, y_variable, x_axis_denominator, decomposition_reform, 
#            decomposition_reference, count, nb_smic_max, stacked}
#    test_case_options : {loyer, loyer_fictif, biactif, categorie_salarie, couple, nb_enfants, 
#            recourant_rsa, zone_apl, statut_occupation_logement ...}



########################
# Compute marginal rates
########################


# Marginal rate computation: set to missing when above 100 or below -100

rates_figure, rates_df = plot_tax_rates(
    baseline_tax_benefit_system = tax_benefit_system,
    tax_benefit_system = tax_benefit_system_reforme,
    period = str(year),
    denominator = varying,
    numerator = 'revenu_disponible',
    tax_numerator = False, # Use False because target (numerator) is an income 
    rates = 'marginal',
    round_unit = 1, # need to have a low unit for accurate results.
    graph_options = {'nb_smic_max': x,
                    'count' : 100},
    test_case_options = household_compositions[household_composition],
)


########################################################

def plot_tax_rates_on_one_variable(
    tax_benefit_system = tax_benefit_system,
    tax_benefit_system_reforme = tax_benefit_system_reforme,
    year = year,
    varying = varying,
    target_variable = 'rsa',
    tax_numerator = True, # Use False because target (numerator) is an income 
    rates = 'marginal',
    round_unit = 1,
    graph_options = {'nb_smic_max': 4},
    test_case_options = household_compositions[household_composition],
    social_contributions_muted = social_contributions_muted,
    ):
    '''
    Function embedding plot tax rates
    '''
    rates_figure, rates_df = plot_tax_rates(
        baseline_tax_benefit_system = tax_benefit_system,
        tax_benefit_system = tax_benefit_system_reforme,
        period = str(year),
        denominator = varying,
        numerator = target_variable,
        tax_numerator = tax_numerator, # Use False if target (numerator) is an income 
        rates = rates,
        round_unit = round_unit,
        graph_options = graph_options,
        test_case_options = test_case_options,
    )
    return rates_figure, rates_df
#############


for target_variable in ['rsa', 'rsa_activite', 'ppe', 'irpp']:
    plot_tax_rates_on_one_variable(
    tax_benefit_system = tax_benefit_system,
    tax_benefit_system_reforme = tax_benefit_system_reforme,
    year = year,
    varying = varying,
    target_variable = target_variable,
    tax_numerator = True, 
    rates = 'marginal',
    round_unit = 1,
    graph_options = {'nb_smic_max': 4},
    test_case_options = household_compositions[household_composition],
    social_contributions_muted = social_contributions_muted,
    )

# supplementary round on large income scale for IRPP
plot_tax_rates_on_one_variable(
    tax_benefit_system = tax_benefit_system,
    tax_benefit_system_reforme = tax_benefit_system_reforme,
    year = str(year),
    varying = varying,
    target_variable = 'irpp',
    tax_numerator = True, 
    rates = 'marginal',
    round_unit = 1,
    graph_options = {'nb_smic_max': 7},
    test_case_options = household_compositions[household_composition],
    social_contributions_muted = social_contributions_muted,
    )
# supplementary round on large income scale for IRPP
plot_tax_rates_on_one_variable(
    tax_benefit_system = tax_benefit_system,
    tax_benefit_system_reforme = tax_benefit_system_reforme,
    year = str(year),
    varying = 'rni',
    target_variable = 'irpp',
    tax_numerator = True, 
    rates = 'marginal',
    round_unit = 2,
    graph_options = {'nb_smic_max': 7},
    test_case_options = household_compositions[household_composition],
    social_contributions_muted = social_contributions_muted,
    )

###Cheking which reform drives changes


# removing rsa only
rates_figure, rates_df = plot_tax_rates_on_one_variable(
    tax_benefit_system = tax_benefit_system,
    tax_benefit_system_reforme = tax_benefit_system_reforme,
    year = str(year),
    varying = varying,
    target_variable = 'revenu_disponible',
    tax_numerator = False, 
    rates = 'marginal',
    round_unit = 1,
    graph_options = {'nb_smic_max': 7},
    test_case_options = household_compositions[household_composition],
    )

###Clean save 



rates_df.to_csv(data_output_path + path_varying[varying]+ "emtr_"+household_composition+"_"+str(year)+
                     "_"+case_filename+"_"+
                     sc_muting_note[social_contributions_muted]+".csv", sep = ';')



