*stop! //avoid running everything by mistake

*********************************************
* Preparation of the database to derive test functions	*
* Compute densities *
*********************************************

*if 	"`c(username)'" == "pablorodriguez" {
*	global path "/Users/pablorodriguez/codes_stage_crest"
*	
*}
*cd "${path}"

global path "/Users/pablorodriguez/Desktop/codes_stage_crest"
cd "${path}"


cap program drop collapse_by_foyer_fiscal_emtr
* only applies to foyer_fiscal level variables
program define collapse_by_foyer_fiscal_emtr 
#delim ;
collapse (mean)  
mean_ff_age = age
revenu_disponible_ff
 weight_foyers
  nb_pac
rbg nb_adult rng  rfr  nbptr
rni irpp  ppe 
(sum) retraite_brute chomage_brut revenus_nets_du_travail 
traitement_indiciaire_brut salaire_de_base salaire_imposable
weight_individus revenus_nets_du_capital
(max) max_ff_age = age, 
by(foyer_fiscal_id foyer_impose maries_ou_pacses celibataire_ou_divorce); 
#delim cr 
end  
// temporarily muted: revenu_categoriel_tspr revenu_categoriel_plus_values credits_impot
// revenu_categoriel_capital revenu_categoriel_non_salarial revenu_categoriel
// revenu_categoriel_foncier

// all elements in key are defined at the foyer fiscal level
// menage_id famille_id removed

/* // variables below defined at the menage level
revenus_capitaux_mobiliers_plus_ revenus_super_bruts_menage
revenus_travail_super_bruts_mena revenus_fonciers_bruts_menage
revenus_remplacement_pensions_br
*/
// variables in the (sum) are defined at the individual level
// variables in the (mean) defined at the foyer fiscal level


*foreach h of numlist 2/4 {

*global year = 2010 + `h'
//


*global year = 2014
*global year = 2015
*global year = 2013
global year = 2012
*****************************************************************************
* Run in one row until next horizontal line

* Get the MTR data
*******************
*Note : we are looking at "postref" which means TBS and data of the same year n
* (behavior has adjusted to legislation)
* we could also look at "preref", same thing

* MTR on IRPP
local baseline False
insheet using "${path}/data_output/output_MTR_postref_TBS${year}_data${year}_baseline`baseline'.csv", clear delimiter(;)
*insheet using "/Users/pablorodriguez/Desktop/codes_stage_crestoutput_MTR_postref_TBS${year}_data${year}_baselineFalse.csv", clear delimiter(;)
unique foyer_fiscal_id	
collapse (mean) mtr_irpp_salaire_de_base ir_taux_marginal ir_tranche irpp_from_mtr, by(foyer_fiscal_id)
tempfile MTR_effective
save `MTR_effective'

* MtR on PPE
local baseline False
insheet using "${path}/data_output/output_MTRppe_postref_TBS${year}_data${year}_baseline`baseline'.csv", clear delimiter(;)
*insheet using "/Users/pablorodriguez/Desktop/codes_stage_crestoutput_MTRppe_postref_TBS${year}_data${year}_baselineFalse.csv", clear delimiter(;)
unique foyer_fiscal_id
//list mtr_ppe_rni if missing(real(mtr_ppe_rni))	
if "$year" == "2011" {
	replace mtr_ppe_rni = "" if mtr_ppe_rni == "-inf"
	destring mtr_ppe_rni, replace
}
collapse (mean) mtr_ppe_rni rni ppe_brute, by(foyer_fiscal_id)
rename rni rni_check_from_mtrppe
tempfile MTRppe_effective
save `MTRppe_effective'

** MTR on RSA full amount
local baseline False
insheet using "${path}/data_output/output_MTRrsa_montant_postref_TBS${year}_data${year}_baseline`baseline'.csv", clear delimiter(;)
*insheet using "/Users/pablorodriguez/Desktop/codes_stage_crestoutput_MTRrsa_montant_postref_TBS${year}_data${year}_baselineFalse.csv", clear delimiter(;)
rename v2 mtr_rsa_montant_rni 
*replace mtr_rsa_montant_rni = "" if mtr_rsa_montant_rni == "-inf"
destring mtr_rsa_montant_rni, replace
collapse (mean) mtr_rsa_montant_rni rni rsa rsa_socle rsa_activite /// 
	prestations_sociales prestations_familiales minima_sociaux /// 
	(sum) rsa_activite_individu, by(foyer_fiscal_id)
rename rni rni_check_from_rsa_montant
tempfile MTRrsa_montant
save `MTRrsa_montant'

* MTR on RSA socle (mostly 1), useful for checks
local baseline False
insheet using "${path}/data_output/output_MTRrsa_socle_postref_TBS${year}_data${year}_baseline`baseline'.csv", clear delimiter(;)
*insheet using "/Users/pablorodriguez/Desktop/codes_stage_crestoutput_MTRrsa_socle_postref_TBS${year}_data${year}_baselineFalse.csv", clear delimiter(;)
rename v2 mtr_rsa_socle_rni 
// variable is already numeric because only numeric values
collapse (mean) mtr_rsa_socle_rni rni, by(foyer_fiscal_id)
rename rni rni_check_from_rsa_socle
tempfile MTRrsa_socle
save `MTRrsa_socle'

* MTR on RSA activite should correspond broadly to MTR on full amount
local baseline False
insheet using "${path}/data_output/output_MTRrsa_act_postref_TBS${year}_data${year}_baseline`baseline'.csv", clear delimiter(;)
*insheet using "/Users/pablorodriguez/Desktop/codes_stage_crestoutput_MTRrsa_act_postref_TBS${year}_data${year}_baselineFalse.csv", clear delimiter(;)
rename v2 mtr_rsa_activite_rni 
//list mtr_rsa_activite_rni if missing(real(mtr_rsa_activite_rni))
*replace mtr_rsa_activite_rni = "" if mtr_rsa_activite_rni == "-inf"
*replace mtr_rsa_activite_rni = "" if mtr_rsa_activite_rni == "inf"
destring mtr_rsa_activite_rni, replace
collapse (mean) mtr_rsa_activite_rni rni, by(foyer_fiscal_id)
rename rni rni_check_from_rsa_act
tempfile MTRrsa_activite
save `MTRrsa_activite'

* Get most of data
***********************
local baseline False
insheet using "${path}/data_output/output_postref_TBS${year}_data${year}_baselineFalse.csv", clear delimiter(;)
*insheet using "/Users/pablorodriguez/Desktop/codes_stage_crestoutput_postref_TBS${year}_data${year}_baselineFalse.csv", clear delimiter(;)


unique foyer_fiscal_id
unique menage_id
unique foyer_fiscal_id menage_id

/*bysort menage_id : egen sd_foyer_fiscal_id = sd(foyer_fiscal_id)
sum sd_foyer_fiscal_id, d
bysort foyer_fiscal_id : egen sd_menage_id = sd(menage_id)
sum sd_menage_id, d
---> there is always 1 menage per foyer fiscal
in other words, one menage may include several foyers fiscaux.
*/
order foyer_fiscal_id menage_id revenu_disponible 
// revenu_disponible is clearly defined at the household level (menage)
// but most other variables are at the foyer fiscal level

preserve
tempfile nb_foyers_fiscaux_per_menage
egen tag_foyer_fiscal = tag(foyer_fiscal_id)
egen tag_menage = tag(menage_id)
keep if tag_foyer_fiscal == 1 | tag_menage == 1
bysort menage_id: gen nb_foyers_fiscaux = _N
order foyer_fiscal_id menage_id revenu_disponible nb_foyers_fiscaux
keep if tag_menage == 1
keep menage_id nb_foyers_fiscaux
save `nb_foyers_fiscaux_per_menage'
restore

merge m:1 menage_id using `nb_foyers_fiscaux_per_menage'

order foyer_fiscal_id menage_id revenu_disponible nb_foyers_fiscaux
gen revenu_disponible_ff = revenu_disponible / nb_foyers_fiscaux

// small checks on salaire de base / salaire imposable
sum salaire_de_base, d
sum salaire_imposable, d
count if salaire_imposable != 0
gen diff_salaire_imposable_de_base = salaire_imposable - salaire_de_base
sum diff_salaire_imposable_de_base , d
// salaire_imposable is always smaller than salaire de base

* Identify age of adults and isolate in separate db
tempfile age_adults
preserve
drop if foyer_fiscal_role == 2 // drop kids
bysort foyer_fiscal_id : egen max_age = max(age)
bysort foyer_fiscal_id : egen min_age = min(age)
bysort foyer_fiscal_id : gen n_obs = _n
drop if n_obs != 1
keep foyer_fiscal_id min_age max_age
save `age_adults'
restore

* Collapse to foyer fiscal level and merge with MTR data
collapse_by_foyer_fiscal_emtr
merge m:1 foyer_fiscal_id using `MTR_effective', // keepusing(irpp_alone MTR_IRPP_RNI)

count if _merge != 3
local check_result = r(N)
if `check_result' != 0 {
	stop // Merge is not good !! discrepancy between MTR file and general file
}
drop _merge

* Add MTR from PPE
merge m:1 foyer_fiscal_id using `MTRppe_effective', // keepusing(irpp_alone MTR_IRPP_RNI)
count if _merge != 3
local check_result = r(N)
if `check_result' != 0 {
	stop // Merge is not good !! discrepancy between MTR file and general file
}
drop _merge

*Add MTR from RSA montant
merge m:1 foyer_fiscal_id using `MTRrsa_montant'
count if _merge != 3
local check_result = r(N)
if `check_result' != 0 {
	stop // Merge is not good !! discrepancy between MTR file and general file
}
drop _merge

*Add MTR from RSA socle
merge m:1 foyer_fiscal_id using `MTRrsa_socle'
count if _merge != 3
local check_result = r(N)
if `check_result' != 0 {
	stop // Merge is not good !! discrepancy between MTR file and general file
}
drop _merge

*Add MTR from RSA activite
merge m:1 foyer_fiscal_id using `MTRrsa_activite'
count if _merge != 3
local check_result = r(N)
if `check_result' != 0 {
	stop // Merge is not good !! discrepancy between MTR file and general file
}
drop _merge

gen check = ((rni == rni_check_from_rsa_montant) & (rni == rni_check_from_mtrppe))
ta check // always 1, good
drop check

* Add age statistics, rigorously computed in a separate file
merge m:1 foyer_fiscal_id using `age_adults'
drop _merge

*****************************************************************************

* RENAMING (for compatibility with Andreas Peichl's programs
gen tax = - irpp // fiitax
gen tax2 = tax // (fiitax+siitax+fica)
gen atr = (tax / rbg ) * 100
gen atr2 = (tax2 / rfr ) * 100

* The MTR is computed in TAXIPP in a very specific way
* copied from openfisca_survey_manager:
* 	marginal_rate = 1 - (modified_target - target) / (modified_varying - varying)
gen correction_term = -1
// correction for IRPP is - 1 + mtr because IRPP is a negative variable !
gen mtr_irpp = correction_term + mtr_irpp_salaire_de_base // it was frate in taxsim
replace mtr_irpp = mtr_irpp * 100


***********************
* Saving
	save  "${path}/EMTR_study_${year}.dta", replace
	use "${path}/EMTR_study_${year}.dta", clear
	

***********************
	
** Apply age restriction: keep about half of sample
count if max_age > 55 & max_age != .
count if min_age < 25 & min_age != .
count if max_age > 60
count if max_age < 25

unique foyer_fiscal_id
count if max_age <= 55 & min_age >= 25 

sum mean_ff_age if max_age > 55, d
sum mean_ff_age if max_age > 55 & nb_adult == 2, d

******* ----> Apply the restriction
keep if max_age <= 55 & min_age >= 25
*****************************************************************************

***********************
** Compute RNI density for each household type
***********************
sort rni
*cap drop estpoint density
*kdensity rni if rni < 100000, generate(estpoint density)

local condition_names adult1_child0 adult1_child1 adult2_child0 adult2_child1

foreach c of numlist 1/4 {

	global condition_name `: word `c' of `condition_names''
	di "${condition_name}"

if "$condition_name" == "adult1_child1" {
	local condition nb_pac == 1 & nb_adult == 1 
}
else if "$condition_name"  == "adult1_child0" {
	local condition nb_pac == 0 & nb_adult == 1 
}
else if "$condition_name" == "adult2_child1" {
	local condition nb_pac == 1 & nb_adult == 2 
}
else if "$condition_name" == "adult2_child0" {
	local condition nb_pac == 0 & nb_adult == 2 
}
	cap drop at
	cap drop cdf1 pdf1
	cap drop _merge
	
	* New PDF/CDF with 0 excluded
		gen income_equal_to_0 = (rni == 0)
		ta income_equal_to_0
		sum income_equal_to_0, d
		global share_0 = r(mean)
		di "$share_0"
	
	preserve 
		keep if income_equal_to_0 == 0
		range at 1 100000 1000 // 10000 20000    
		akdensity rni if `condition', at(at) cdf(cdf1) gen(pdf1), [aw=weight_foyers]
		tempfile f_${condition_name}
		keep at cdf1 pdf1
		gen est = 1
		rename at rni
		drop if mi(rni)
		save `f_${condition_name}', replace 
	restore
	append using `f_${condition_name}' 

	sort rni
	ipolate cdf1 rni, gen(CDFipolate_${condition_name})
	ipolate pdf1 rni, gen(PDFipolate_${condition_name})
	lab var CDFipolate_${condition_name} "CDF"
	lab var PDFipolate_${condition_name} "PDF"
	drop if est == 1
	drop est
	drop cdf1 pdf1 income_equal_to_0

	** Correction for zeros
		sum PDFipolate_${condition_name} if rni == 0, d
		sum CDFipolate_${condition_name} if rni == 0, d
		replace CDFipolate_${condition_name} = (1 - $share_0) * CDFipolate_${condition_name} + $share_0
		replace PDFipolate_${condition_name} = (1 - $share_0) * PDFipolate_${condition_name}
	
	* Test
	scatter PDFipolate_${condition_name} rni if rni < 100000

}
***********************
local condition_names adult1_child0 adult1_child1 adult2_child0 adult2_child1

foreach c of numlist 1/4 {
	global condition_name `: word `c' of `condition_names''
	di "${condition_name}"
if "$condition_name" == "adult1_child1" {
	local condition nb_pac == 1 & nb_adult == 1 
}
else if "$condition_name"  == "adult1_child0" {
	local condition nb_pac == 0 & nb_adult == 1 
}
else if "$condition_name" == "adult2_child1" {
	local condition nb_pac == 1 & nb_adult == 2 
}
else if "$condition_name" == "adult2_child0" {
	local condition nb_pac == 0 & nb_adult == 2 
}
	cap drop at
	cap drop cdf1 pdf1
	cap drop _merge

	* New PDF/CDF with 0 excluded
		gen income_equal_to_0 = (salaire_de_base == 0)
		ta income_equal_to_0
		sum income_equal_to_0, d
		global share_0 = r(mean)
		di "$share_0"
	
	preserve 
		keep if income_equal_to_0 == 0
		range at 1 100000 1000 // 10000 20000    
		akdensity salaire_de_base if `condition', at(at) cdf(cdf1) gen(pdf1), [aw=weight_foyers]
		tempfile f_${condition_name}
		keep at cdf1 pdf1
		gen est = 1
		rename at salaire_de_base
		drop if mi(salaire_de_base)
		save `f_${condition_name}', replace 
	restore
	append using `f_${condition_name}' 

	sort salaire_de_base
	ipolate cdf1 salaire_de_base, gen(CDFipol_sdb_${condition_name})
	ipolate pdf1 salaire_de_base, gen(PDFipol_sdb_${condition_name})
	lab var CDFipol_sdb_${condition_name} "CDF"
	lab var PDFipol_sdb_${condition_name} "PDF"
	drop if est == 1
	drop cdf1 pdf1 income_equal_to_0
	
	** Correction for zeros
		sum PDFipol_sdb_${condition_name} if salaire_de_base == 0, d
		sum CDFipol_sdb_${condition_name} if salaire_de_base == 0, d
		replace CDFipol_sdb_${condition_name} = (1 - $share_0) * CDFipol_sdb_${condition_name} + $share_0
		replace PDFipol_sdb_${condition_name} = (1 - $share_0) * PDFipol_sdb_${condition_name}

* Test
	scatter CDFipol_sdb_${condition_name} salaire_de_base if salaire_de_base < 100000
}

**********************
* Saving
	save  "${path}/EMTR_study_withdensities_${year}.dta", replace
	use "${path}/EMTR_study_withdensities_${year}.dta", clear
***********************	
	


*********************
* Plot EMTR on IRPP
*********************
	
*binscatter mtr_irpp rni
*binscatter mtr_irpp rni if nb_pac == 1 & nb_adult == 1
#delim ;
twoway (scatter mtr_irpp rni if nb_pac == 0 & nb_adult == 1 & rni < 100000, 
			yaxis(1) msize(vsmall))
	(connected CDFipolate_adult1_child0 rni if nb_pac == 0 & nb_adult == 1 & rni < 100000, 
		yaxis(2) yscale(alt axis(2)) mcolor(gs7) lcolor(gs7) msize(tiny))
	(connected PDFipolate_adult1_child0 rni if nb_pac == 0 & nb_adult == 1 & rni < 100000, 
		yaxis(3) yscale(off axis(3)) mcolor(gs11) lcolor(gs11) msize(tiny)),
name(adult1_kid0, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("EMTR for IRPP, ${year} taxes and data" "One adult and no child")
note("EMTR is simulated with variations of the net taxable income (RNI, Revenu Net Imposable).");
graph export "${path}/graphs/EMTR/EMTR_adult1_child0_${year}.png", replace;
*graph export "/Users/pablorodriguez/Desktop/codes_stage_crest/EMTR_adult1_child0_${year}.png", replace;
#delim cr

#delim ;
twoway (scatter mtr_irpp rni if nb_pac == 1 & nb_adult == 1 & rni < 100000,
			yaxis(1) msize(vsmall))
	(connected CDFipolate_adult1_child1 rni if nb_pac == 1 & nb_adult == 1 & rni < 100000, 
		yaxis(2) yscale(alt axis(2)) mcolor(gs7) lcolor(gs7) msize(tiny))
	(connected PDFipolate_adult1_child1 rni if nb_pac == 1 & nb_adult == 1 & rni < 100000, 
		yaxis(3) yscale(off axis(3)) mcolor(gs11) lcolor(gs11) msize(tiny)),
name(adult1_kid1, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("EMTR for IRPP, ${year} taxes and data" "One adult and one child")
note("EMTR is simulated with variations of the net taxable income (RNI, Revenu Net Imposable).");
graph export "${path}/graphs/EMTR/EMTR_adult1_child1_${year}.png", replace;
*graph export "/Users/pablorodriguez/Desktop/codes_stage_crest/EMTR_adult1_child1_${year}.png", replace;
#delim cr

#delim ;
twoway (scatter mtr_irpp rni if nb_pac == 0 & nb_adult == 2 & rni < 100000, 
			yaxis(1) msize(vsmall))
	(connected CDFipolate_adult2_child0 rni if nb_pac == 0 & nb_adult == 2 & rni < 100000, 
		yaxis(2) yscale(alt axis(2)) mcolor(gs7) lcolor(gs7) msize(tiny))
	(connected PDFipolate_adult2_child0 rni if nb_pac == 0 & nb_adult == 2 & rni < 100000, 
		yaxis(3) yscale(off axis(3)) mcolor(gs11) lcolor(gs11) msize(tiny)),
name(adult2_kid0, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("EMTR for IRPP, ${year} taxes and data" "Two adults and no child")
note("EMTR is simulated with variations of the net taxable income (RNI, Revenu Net Imposable).");
graph export "${path}/graphs/EMTR/EMTR_adult2_child0_${year}.png", replace;
*graph export "/Users/pablorodriguez/Desktop/codes_stage_crest/EMTR_adult2_child0_${year}.png", replace;
#delim cr

#delim ;
twoway (scatter mtr_irpp rni if nb_pac == 1 & nb_adult == 2 & rni < 100000,
			yaxis(1) msize(vsmall))
	(connected CDFipolate_adult2_child1 rni if nb_pac == 1 & nb_adult == 2 & rni < 100000, 
		yaxis(2) yscale(alt axis(2)) mcolor(gs7) lcolor(gs7) msize(tiny))
	(connected PDFipolate_adult2_child1 rni if nb_pac == 1 & nb_adult == 2 & rni < 100000, 
		yaxis(3) yscale(off axis(3)) mcolor(gs11) lcolor(gs11) msize(tiny)),
name(adult2_kid1, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("EMTR for IRPP, ${year} taxes and data" "Two adults and one child")
note("EMTR is simulated with variations of the net taxable income (RNI, Revenu Net Imposable).");
graph export "${path}/graphs/EMTR/EMTR_adult2_child1_${year}.png", replace;
*graph export "/Users/pablorodriguez/Desktop/codes_stage_crest/EMTR_adult2_child1_${year}.pdf", replace;
#delim cr



*********************
* Plot EMTR on IRPP for people with and without ppe
****************

count if ppe != 0
gen ppe_received = (ppe != 0 & ppe != .)
ta ppe_received

gen chomage = (chomage_brut != 0)
ta chomage
ta chomage ppe_received // not directly correlated

*without density
#delim ;
twoway (scatter mtr_irpp rni if nb_pac == 1 & nb_adult == 1 & rni < 100000 & ppe_received == 0, msize(vsmall))
	(scatter mtr_irpp rni if nb_pac == 1 & nb_adult == 1 & rni < 100000 & ppe_received == 1, msize(vsmall) mcolor(eltblue)),
legend( order(1 "No PPE" 2 "PPE received"))
name(adult1, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("EMTR for IRPP, ${year} taxes and data" "One adult and one child")
note("EMTR is simulated with variations of the net taxable income (RNI, Revenu Net Imposable).");
graph export "${path}/graphs/EMTR/EMTR_adult1_child1_ppeYN_${year}.png", replace;
*graph export "/Users/pablorodriguez/Desktop/codes_stage_crest/EMTR_adult1_child1_ppeYN_${year}.png", replace;

#delim cr

* with density
#delim ;
twoway (scatter mtr_irpp rni if nb_pac == 1 & nb_adult == 1 & rni < 100000 & ppe_received == 0, msize(vsmall) yaxis(1))
	(scatter mtr_irpp rni if nb_pac == 1 & nb_adult == 1 & rni < 100000 & ppe_received == 1, yaxis(1) msize(vsmall) mcolor(eltblue))
	(connected PDFipolate_adult1_child1 rni if nb_pac == 1 & nb_adult == 1 & rni < 100000, yaxis(2) mcolor(gs11) msize(tiny)),
legend( order(1 "No PPE" 2 "PPE received" 3 "Income density"))
name(density, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("EMTR for IRPP, ${year} taxes and data" "One adult and one child")
note("EMTR is simulated with variations of the net taxable income (RNI, Revenu Net Imposable).");
graph export "${path}/graphs/EMTR/EMTR_adult1_child1_ppeYN_RNIdensity_${year}.png", replace;
*graph export "/Users/pablorodriguez/Desktop/codes_stage_crest/EMTR_adult1_child1_ppeYN_RNIdensity_${year}.png", replace;

#delim cr


** Add specific condition (not unemployed + age condition)
* need to recompute the density
global condition_household nb_pac == 1 & nb_adult == 1 & max_age <= 55 & max_age >= 25
global condition_incomes rni < 100000 & chomage == 0
global condition $condition_household & $condition_incomes
* Recompute density
	global condition_name condition_specific
	range at 0 100000 1000 // 10000 20000    
	cap drop _merge
	akdensity rni if $condition, at(at) cdf(cdf1) gen(pdf1), [aw=weight_foyers]
	preserve 
	tempfile f_${condition_name}
	keep at cdf1 pdf1
	gen est = 1
	rename at rni
	drop if mi(rni)
	save `f_${condition_name}', replace 
	restore
	drop at cdf1 pdf1
	append using `f_${condition_name}' 
	sort rni
	ipolate cdf1 rni, gen(CDFipolate_${condition_name})
	ipolate pdf1 rni, gen(PDFipolate_${condition_name})
	drop if est == 1
	drop cdf1 pdf1
#delim ;
twoway (scatter mtr_irpp rni if $condition & ppe_received == 0, msize(vsmall))
	(scatter mtr_irpp rni if $condition & ppe_received == 1, msize(vsmall) mcolor(eltblue))
	(connected PDFipolate_condition_specific rni if $condition , yaxis(2) mcolor(gs11) lcolor(gs11) msize(tiny)),
legend( order(1 "No PPE" 2 "PPE received" 3 "Income density"))
name(adult1, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("EMTR for IRPP, ${year} taxes and data" "One adult and one child, with sample restrictions")
note("EMTR is simulated with variations of the net taxable income (RNI, Revenu Net Imposable)."
"Restricted to individuals between 25 and 55, not unemployed.");
graph export "${path}/graphs/EMTR/EMTR_adult1_child1_ppeYN_${year}_noU_agecondition.png", replace;
*graph export "/Users/pablorodriguez/Desktop/codes_stage_crest/EMTR_adult1_child1_ppaYN_${year}_noU_agecondition.png", replace;
#delim cr


****************************************** 
* Average ber income bin
*****************************************
cap drop rni_rounded
gen rni_rounded = round(rni, 500)
ta rni_rounded if rni < 10000

cap drop _merge
cap drop rounded_mtr_adult1_child0
tempfile rni_rounded_adult1_child0
preserve
keep if nb_pac == 0 & nb_adult == 1 & rni < 100000
collapse mtr_irpp, by(rni_rounded)
rename mtr_irpp rounded_mtr_adult1_child0
save `rni_rounded_adult1_child0'
restore
merge m:1 rni_rounded using `rni_rounded_adult1_child0'
#delim ;
twoway (scatter rounded_mtr_adult1_child0 rni if nb_pac == 0 & nb_adult == 1 & rni < 100000, yaxis(1) msize(vsmall))
	(connected PDFipolate_adult1_child0 rni if nb_pac == 0 & nb_adult == 1 & rni < 100000, yaxis(2) mcolor(gs11) lcolor(gs11) msize(tiny)),
name(adult1_kid0_avg, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("Avg. EMTR for IRPP, ${year} taxes and data" "One adult and no child")
note("EMTR is simulated with variations of the net taxable income (RNI, Revenu Net Imposable)."
"Density is an interpolated kernel estimation." 
"EMTR is an average over 500 euros windows.");
graph export "${path}/graphs/EMTR/avgEMTR_adult1_child0_${year}.png", replace;
*graph export "/Users/pablorodriguez/Desktop/codes_stage_crest/avgEMTR_adult1_child0_${year}.png", replace;
#delim cr

cap drop _merge
cap drop rounded_mtr_adult1_child1
tempfile rni_rounded_adult1_child1
preserve
keep if nb_pac == 1 & nb_adult == 1 & rni < 100000
collapse mtr_irpp, by(rni_rounded)
rename mtr_irpp rounded_mtr_adult1_child1
save `rni_rounded_adult1_child1'
restore
merge m:1 rni_rounded using `rni_rounded_adult1_child1'
#delim ;
twoway (scatter rounded_mtr_adult1_child1 rni if nb_pac == 0 & nb_adult == 1 & rni < 100000, yaxis(1) msize(vsmall))
	(connected PDFipolate_adult1_child1 rni if nb_pac == 0 & nb_adult == 1 & rni < 100000, yaxis(2) mcolor(gs11) lcolor(gs11) msize(tiny)),
name(adult1_kid1_avg, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("Avg. EMTR for IRPP, ${year} taxes and data" "One adult and one child")
note("EMTR is simulated with variations of the net taxable income (RNI, Revenu Net Imposable)."
"Density is an interpolated kernel estimation." 
"EMTR is an average over 500 euros windows.");
graph export "${path}/graphs/EMTR/avgEMTR_adult1_child1_${year}.png", replace;
*graph export "/Users/pablorodriguez/Desktop/codes_stage_crest/avgEMTR_adult1_child1_${year}.png", replace;
#delim cr


*******************************
* Look precisely at PPE
*******************************

#delim ;
scatter ppe rni if nb_pac == 0 & nb_adult == 1 & rni < 100000 & ppe != 0, msize(tiny)
name(ppe_rni_child0, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("PPE depending on RNI, ${year} taxes and data" "One adult and no child")
note("X variable is net taxable income (RNI, Revenu Net Imposable)."
"Y variable is total ppe received by foyer fiscal.");
graph export "${path}/graphs/EMTR/ppe_rni_adult1_child0_${year}.png", replace;
*graph export "/Users/pablorodriguez/Desktop/codes_stage_crest/ppe_rni_adult1_child0_${year}.png", replace;
#delim cr

#delim ;
scatter ppe rni if nb_pac == 1 & nb_adult == 1 & rni < 100000 & ppe != 0, msize(tiny)
name(ppe_rni_child1, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("PPE depending on RNI, ${year} taxes and data" "One adult and one child")
note("X variable is net taxable income (RNI, Revenu Net Imposable)."
"Y variable is total ppe received by foyer fiscal.");
graph export "${path}/graphs/EMTR/ppe_rni_adult1_child1_${year}.pdf", replace;
*graph export "/Users/pablorodriguez/Desktop/codes_stage_crest/ppe_rni_adult1_child1_${year}.png", replace;
#delim cr

#delim ;
scatter ppe revenus_nets_du_travail if nb_pac == 0 & nb_adult == 1 & rni < 100000 & ppe != 0 
	& max_age <= 55 & max_age >= 25, msize(tiny)
name(ppe_revenus_nets_du_travail, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("PPE depending on net work income, ${year} taxes and data" "One adult and no child")
// subtitle("Age between 25 and 55")
note("X variable is net income from work."
"Y variable is total ppe received by foyer fiscal.");
graph export "${path}/graphs/EMTR/ppe_revtravail_adult1_child0_${year}.png", replace;
*graph export "/Users/pablorodriguez/Desktop/codes_stage_crest/ppe_revtravail_adult1_child0_${year}.png", replace;
#delim cr


