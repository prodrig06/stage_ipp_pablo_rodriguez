# -*- coding: utf-8 -*-
"""


@author: Pablo Rodriguez

Code computes effective marginal tax rates on tax and transfer system from actual 
French taxpayers
1) Reforms coding (allows to create some variables), apply them to TBS
2) Create dataframe with emtr
3) Plot boxplots
4) Plot emtr by quantiles
5) Plot kernel density of emtr

"""

#############################
# Very important remarks on this code 

# For an unknown reason when running the code, some graphs overlap with each other .
# In order to obtain the desired output, one must run the code first and then
# run individually the following lines, corresponding to the plotting functions:
# Line 651: boxplot with outliers
# Line 696: boxplot without outliers
# Line 766: Plots P10, P25, P50, P75, P95 of the mean marginal tax rate by quantile
# Line 830: plots the mean marginal tax rate by quantile, as well as scatter plots
# of the effective marginal tax rates
# Line 886: mean marginal tax rate by quantile
# Line 932: kernel density of the emtr

#############################

import copy
import pandas as pd
import numpy
import weightedcalcs as wc
import seaborn as sns


from taxipp.base import taxipp_tax_and_benefit_system
from openfisca_survey_manager.variables import create_quantile
from openfisca_france_data import france_data_tax_benefit_system
from openfisca_france_data.erfs_fpr.get_survey_scenario import get_survey_scenario
from openfisca_core.rates import average_rate
from openfisca_france.model.base import *
from numpy import around
from openfisca_core.reforms import Reform
from taxipp.utils.redistribution_utils import graph_par_quantile
from taxipp.utils.general_utils import to_percent_round_formatter
from taxipp.utils.general_utils import ipp_colors, label_by_variable
from matplotlib.ticker import FuncFormatter



##Setting TBS
tax_benefit_system = taxipp_tax_and_benefit_system

class create_salaire_imposable_foyers(Reform):
    name = "Salaire imposable foyers"
    def apply(self):
        class salaire_imposable_foyers(Variable):
                value_type = float
                entity = FoyerFiscal
                label = u"Salaires imposables totaux du foyer"
                definition_period = YEAR

                def formula(foyer_fiscal, period, parameters):
                    salaire_imposable_i = foyer_fiscal.members('salaire_imposable', period, options = [ADD])

                    return foyer_fiscal.sum(salaire_imposable_i)
        self.add_variable(salaire_imposable_foyers)


class tax_benefit_system_reform(Reform):
    name = u'Tax benefit system avec réforme'
    # Système en année paiement => revenu disponible = revenus N + prestations reçues en N - impôts payés en N (sur revenus N-1)
    # NB : on modifie seulement les variables de mesures.py (sinon c'est tentaculaire)

    def apply(self):

        # Calcul de la variable avec impôts sur revenus N-1
        class prelevement_forfaitaire_unique_ir_annee_paiement(Variable):
            value_type = float
            entity = FoyerFiscal
            label = u"Notion économique de l'IRPP"
            definition_period = YEAR

            def formula(foyer_fiscal, period, parameters):
                prelevement_forfaitaire_unique_ir_annee_paiement = foyer_fiscal('prelevement_forfaitaire_unique_ir', period.last_year)

                return prelevement_forfaitaire_unique_ir_annee_paiement

        class prelevement_forfaitaire_liberatoire_annee_paiement(Variable):
            value_type = float
            entity = FoyerFiscal
            label = u"Notion économique de l'IRPP"
            definition_period = YEAR

            def formula(foyer_fiscal, period, parameters):
                prelevement_forfaitaire_liberatoire_annee_paiement = foyer_fiscal('prelevement_forfaitaire_liberatoire', period.last_year)

                return prelevement_forfaitaire_liberatoire_annee_paiement

        class ir_pv_immo_annee_paiement(Variable):
            value_type = float
            entity = FoyerFiscal
            label = u"Notion économique de l'IRPP"
            definition_period = YEAR

            def formula(foyer_fiscal, period, parameters):
                ir_pv_immo_annee_paiement = foyer_fiscal('ir_pv_immo', period.last_year)

                return ir_pv_immo_annee_paiement



        class irpp_economique_annee_paiement(Variable):
            value_type = float
            entity = FoyerFiscal
            label = u"Notion économique de l'IRPP"
            definition_period = YEAR

            def formula(foyer_fiscal, period, parameters):
                '''
                Cette variable d'IRPP comptabilise dans les montants
                d'imposition les acomptes qui, dans la déclaration fiscale, sont considérés comme des crédits
                d'impôt. Ajouter ces acomptes au montant "administratif" d'impôt correspond donc au "véritable impôt"
                payé en totalité, alors que la variable 'irpp' correspond à une notion administrative.
                Exemple :
                Certains revenus du capital sont soumis à un prélèvement forfaitaire à la source non libératoire,
                faisant office d'acompte. Puis, l'impôt au barème sur ces revenus est calculé, et confronté à l'acompte.
                Cet acompte, est en case 2CK, et considéré comme un crédit d'impôt. Retrancher de l'impôt au barème ce
                crédit permet d'obtenir l'impôt dû suite à la déclaration de revenus, qui correspond à la variable 'irpp'.
                Cette notion est administrative. L'impôt total payé correspond à cette notion administrative, augmentée des acomptes.
                A partir de 2019 : on calcule toujours l'impôt de l'année N
                sur les revenus N-1, mais ce calcul sert juste à calculer
                un taux moyen, appliqué aux revenus 2019. Donc, on multiplie juste
                l'irpp par le taux de vieillissement utilisé pour vieillir la base
                '''
                irpp = foyer_fiscal('irpp', period.last_year)
                acomptes_ir = foyer_fiscal('acomptes_ir', period.last_year)

                return irpp - acomptes_ir

            def formula_2019_01_01(foyer_fiscal, period, parameters):
                '''
                Cf. doctring première formule
                '''
                irpp = foyer_fiscal('irpp', period.last_year)
                acomptes_ir = foyer_fiscal('acomptes_ir', period.last_year)

                return (irpp - acomptes_ir) * 1.031

        class salaire_de_base_foyers(Variable):
                value_type = float
                entity = FoyerFiscal
                label = "Salaire de base, en général appelé salaire brut, la 1ère ligne sur la fiche de paie"
                definition_period = YEAR

                def formula(foyer_fiscal, period, parameters):
                    salaire_de_base_i = foyer_fiscal.members('salaire_de_base', period, options = [ADD])

                    return foyer_fiscal.sum(salaire_de_base_i)



        class revenus_nets_foyers(Variable):
                value_type = float
                entity = FoyerFiscal
                label = u"Revenus nets du foyer"
                definition_period = YEAR

                def formula(foyer_fiscal, period, parameters):
                    pensions_nettes_i = foyer_fiscal.members('pensions_nettes', period)
                    revenus_nets_du_capital_i = foyer_fiscal.members('revenus_nets_du_capital', period)
                    revenus_nets_du_travail_i = foyer_fiscal.members('revenus_nets_du_travail', period)
                    pensions_nettes = foyer_fiscal.sum(pensions_nettes_i)
                    revenus_nets_du_capital = foyer_fiscal.sum(revenus_nets_du_capital_i)
                    revenus_nets_du_travail = foyer_fiscal.sum(revenus_nets_du_travail_i)

                    impots_directs_annee_paiement = foyer_fiscal('impots_directs_annee_paiement', period)
                    """
                    # On prend en compte les PPE touchés par un foyer fiscal dont le déclarant principal est dans le ménage
                    ppe_i = foyer_fiscal.members.foyer_fiscal('ppe', period)  # PPE du foyer fiscal auquel appartient chaque membre du ménage
                    ppe = foyer_fiscal.sum(ppe_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)  # On somme seulement pour les déclarants principaux
                    """


                    # On prend en compte les prestations sociales touchées par une famille dont le demandeur est dans le ménage
                    prestations_sociales_i = foyer_fiscal.members.famille('prestations_sociales', period)  # PF de la famille auquel appartient chaque membre du ménage
                    prestations_sociales = foyer_fiscal.sum(prestations_sociales_i, role = Famille.DEMANDEUR)  # On somme seulement pour les demandeurs


                    return (
                    revenus_nets_du_travail
                    + impots_directs_annee_paiement
                    + pensions_nettes
                    + prestations_sociales
                    + revenus_nets_du_capital
                    )


        class revenus_nets_f(Variable):
                value_type = float
                entity = FoyerFiscal
                label = u"Revenus nets"
                definition_period = YEAR

                def formula(foyer_fiscal, period, parameters):
                    pensions_nettes_i = foyer_fiscal.members('pensions_nettes', period)
                    revenus_nets_du_capital_i = foyer_fiscal.members('revenus_nets_du_capital', period)
                    revenus_nets_du_travail_i = foyer_fiscal.members('revenus_nets_du_travail', period)

                    revenus_nets = foyer_fiscal.sum(pensions_nettes_i + revenus_nets_du_capital_i + revenus_nets_du_travail_i)
                    return revenus_nets
        # Calcul de l'impôt direct à partir de l'irpp economique en année paiement

        class impots_directs_annee_paiement(Variable):
            value_type = float
            entity = FoyerFiscal
            label = u"Impôts directs"
            reference = "http://fr.wikipedia.org/wiki/Imp%C3%B4t_direct"
            definition_period = YEAR

            def formula(foyer_fiscal, period, parameters):
                
                """

                # On prend en compte l'IR des foyers fiscaux dont le déclarant principal est dans le ménage
                irpp_economique_annee_paiement_i = members.foyer_fiscal('irpp_economique_annee_paiement', period)
                irpp_economique_annee_paiement = foyer_fiscal.sum(irpp_economique_annee_paiement_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                # On prend en compte le PFL des foyers fiscaux dont le déclarant principal est dans le ménage : variable existant jusqu'en 2017
                prelevement_forfaitaire_liberatoire_annee_paiement_i = members.foyer_fiscal('prelevement_forfaitaire_liberatoire_annee_paiement', period)
                prelevement_forfaitaire_liberatoire_annee_paiement = foyer_fiscal.sum(prelevement_forfaitaire_liberatoire_annee_paiement_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                # On prend en compte le PFU (partie au titre de l'IR) des foyers fiscaux dont le déclarant principal est dans le ménage : variable existant à partir de 2018
                prelevement_forfaitaire_unique_ir_annee_paiement_i = members.foyer_fiscal('prelevement_forfaitaire_unique_ir_annee_paiement', period)
                prelevement_forfaitaire_unique_ir_annee_paiement = foyer_fiscal.sum(prelevement_forfaitaire_unique_ir_annee_paiement_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                # On comptabilise ir_pv_immo ici directement, et non pas dans la variable 'irpp', car administrativement, cet impôt n'est pas dans l'irpp, et n'est déclaré dans le formulaire 2042C que pour calculer le revenu fiscal de référence. On colle à la définition administrative, afin d'avoir une variable 'irpp' qui soit comparable à l'IR du simulateur en ligne de la DGFiP
                # On prend en compte l'IR sur PV immobilières des foyers fiscaux dont le déclarant principal est dans le ménage
                ir_pv_immo_annee_paiement_i = members.foyer_fiscal('ir_pv_immo_annee_paiement', period)
                ir_pv_immo_annee_paiement = foyer_fiscal.sum(ir_pv_immo_annee_paiement_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)
                """

                irpp_economique_annee_paiement = foyer_fiscal('irpp_economique_annee_paiement', period)
                prelevement_forfaitaire_liberatoire_annee_paiement = foyer_fiscal('prelevement_forfaitaire_liberatoire_annee_paiement', period)
                prelevement_forfaitaire_unique_ir_annee_paiement = foyer_fiscal('prelevement_forfaitaire_unique_ir_annee_paiement', period)
                ir_pv_immo_annee_paiement = foyer_fiscal('ir_pv_immo_annee_paiement', period)
                return irpp_economique_annee_paiement + prelevement_forfaitaire_liberatoire_annee_paiement + prelevement_forfaitaire_unique_ir_annee_paiement + ir_pv_immo_annee_paiement


        
        class irpp_economique_menage_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Notion économique de l'IRPP"
            definition_period = YEAR

            def formula(menage, period, parameters):
                irpp_i = menage.members.foyer_fiscal('irpp_economique_annee_paiement', period)
                irpp = menage.sum(irpp_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                return irpp

        class irpp_economique_menage_par_uc_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Notion économique de l'IRPP"
            definition_period = YEAR

            def formula(menage, period, parameters):
                irpp_economique = menage('irpp_economique_menage_annee_paiement', period)
                uc = menage('unites_consommation', period)

                return irpp_economique / uc

        class ir_brut_menage_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Impôt sur le revenu brut avant non imposabilité et plafonnement du quotient"
            definition_period = YEAR

            def formula(menage, period, parameters):
                ir_brut_i = menage.members.foyer_fiscal('ir_brut', period.last_year)
                ir_brut_menage = menage.sum(ir_brut_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                return ir_brut_menage

        class ir_brut_menage_par_uc_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Impôt sur le revenu brut avant non imposabilité et plafonnement du quotient"
            definition_period = YEAR

            def formula(menage, period, parameters):
                ir_brut = menage('ir_brut_menage_annee_paiement', period)
                uc = menage('unites_consommation', period)

                return ir_brut / uc


        class ir_plaf_qf_menage_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Impôt après plafonnement du quotient familial et réduction complémentaire"
            definition_period = YEAR

            def formula(menage, period, parameters):
                ir_plaf_qf_i = menage.members.foyer_fiscal('ir_plaf_qf', period.last_year)
                ir_plaf_qf_menage = menage.sum(ir_plaf_qf_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                return ir_plaf_qf_menage


        class ip_net_menage_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Impôt sur le revenu après décote et réduction sous condition de revenus, avant réductions"
            definition_period = YEAR

            def formula(menage, period, parameters):
                ip_net_i = menage.members.foyer_fiscal('ip_net', period.last_year)
                ip_net_menage = menage.sum(ip_net_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                return ip_net_menage

        class ip_net_menage_par_uc_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Impôt sur le revenu après décote et réduction sous condition de revenus, avant réductions"
            definition_period = YEAR

            def formula(menage, period, parameters):
                ip_net = menage('ip_net_menage_annee_paiement', period)
                uc = menage('unites_consommation', period)

                return ip_net / uc


        class iaidrdi_menage_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Impôt après imputation des réductions d'impôt"
            definition_period = YEAR

            def formula(menage, period, parameters):
                iaidrdi_i = menage.members.foyer_fiscal('iaidrdi', period.last_year)
                iaidrdi_menage = menage.sum(iaidrdi_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                return iaidrdi_menage


        class iai_menage_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Impôt avant imputations de l'impôt sur le revenu"
            definition_period = YEAR

            def formula(menage, period, parameters):
                iai_i = menage.members.foyer_fiscal('iai', period.last_year)
                iai_menage = menage.sum(iai_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                return iai_menage

        class iai_menage_par_uc_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Impôt avant imputations de l'impôt sur le revenu"
            definition_period = YEAR

            def formula(menage, period, parameters):
                iai = menage('iai_menage_annee_paiement', period)
                uc = menage('unites_consommation', period)

                return iai / uc


        class irpp_menage_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Impôt sur le revenu des personnes physiques restant à payer, après prise en compte des éventuels acomptes"
            definition_period = YEAR

            def formula(menage, period, parameters):
                irpp_i = menage.members.foyer_fiscal('irpp', period.last_year)
                irpp_menage = menage.sum(irpp_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                return irpp_menage

        class irpp_menage_par_uc_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Impôt sur le revenu des personnes physiques restant à payer, après prise en compte des éventuels acomptes"
            definition_period = YEAR

            def formula(menage, period, parameters):
                irpp = menage('irpp_menage_annee_paiement', period)
                uc = menage('unites_consommation', period)

                return irpp / uc

        # Calcul du revenu disponible à partir des impôts en année paiement (impôt payés en N sur revenus N-1)
        class revenu_disponible_menage_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Revenu disponible du ménage"
            reference = "http://fr.wikipedia.org/wiki/Revenu_disponible"
            definition_period = YEAR

            def formula(menage, period, parameters):
                pensions_nettes_i = menage.members('pensions_nettes', period)
                revenus_nets_du_capital_i = menage.members('revenus_nets_du_capital', period)
                revenus_nets_du_travail_i = menage.members('revenus_nets_du_travail', period)
                pensions_nettes = menage.sum(pensions_nettes_i)
                revenus_nets_du_capital = menage.sum(revenus_nets_du_capital_i)
                revenus_nets_du_travail = menage.sum(revenus_nets_du_travail_i)

                impots_directs_menage_annee_paiement = menage('impots_directs_menage_annee_paiement', period)

                # On prend en compte les PPE touchés par un foyer fiscal dont le déclarant principal est dans le ménage
                ppe_i = menage.members.foyer_fiscal('ppe', period)  # PPE du foyer fiscal auquel appartient chaque membre du ménage
                ppe = menage.sum(ppa_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)  # On somme seulement pour les déclarants principaux

                # On prend en compte les prestations sociales touchées par une famille dont le demandeur est dans le ménage
                prestations_sociales_i = menage.members.famille('prestations_sociales', period)  # PF de la famille auquel appartient chaque membre du ménage
                prestations_sociales = menage.sum(prestations_sociales_i, role = Famille.DEMANDEUR)  # On somme seulement pour les demandeurs

                return (
                    revenus_nets_du_travail
                    + impots_directs_menage_annee_paiement
                    + pensions_nettes
                    + ppe
                    + prestations_sociales
                    + revenus_nets_du_capital
                    )


        # Calcul du niveau de vie à partir du revenu disponible en année paiement
        class niveau_de_vie_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Niveau de vie du ménage"
            definition_period = YEAR

            def formula(menage, period):
                revenu_disponible_menage_annee_paiement = menage('revenu_disponible_menage_annee_paiement', period)
                uc = menage('unites_consommation', period)
                return revenu_disponible_menage_annee_paiement / uc


        class centile_niveau_de_vie_annee_paiement(Variable):
            value_type = int
            entity = Menage
            label = u"Centile de niveau de vie année paiement"
            definition_period = YEAR

            def formula(menage, period):
                menage_ordinaire = menage('menage_ordinaire', period)
                revenu_net_menage = menage('niveau_de_vie_annee_paiement', period)
                wprm = menage('wprm', period)
                labels = arange(1, 101)
                method = 2
                if len(wprm) == 1:
                    return wprm * 0
                centile, values = mark_weighted_percentiles(
                    revenu_net_menage, labels, wprm * menage_ordinaire, method, return_quantiles = True)
                del values
                return centile * menage_ordinaire


        # Calcul de l'impôt direct à partir de l'irpp economique en année paiement
        class impots_directs_menage_annee_paiement(Variable):
            value_type = float
            entity = Menage
            label = u"Impôts directs"
            reference = "http://fr.wikipedia.org/wiki/Imp%C3%B4t_direct"
            definition_period = YEAR

            def formula(menage, period, parameters):

                # On prend en compte l'IR des foyers fiscaux dont le déclarant principal est dans le ménage
                irpp_economique_annee_paiement_i = menage.members.foyer_fiscal('irpp_economique_annee_paiement', period)
                irpp_economique_annee_paiement = menage.sum(irpp_economique_annee_paiement_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                # On prend en compte le PFL des foyers fiscaux dont le déclarant principal est dans le ménage : variable existant jusqu'en 2017
                prelevement_forfaitaire_liberatoire_annee_paiement_i = menage.members.foyer_fiscal('prelevement_forfaitaire_liberatoire_annee_paiement', period)
                prelevement_forfaitaire_liberatoire_annee_paiement = menage.sum(prelevement_forfaitaire_liberatoire_annee_paiement_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                # On prend en compte le PFU (partie au titre de l'IR) des foyers fiscaux dont le déclarant principal est dans le ménage : variable existant à partir de 2018
                prelevement_forfaitaire_unique_ir_annee_paiement_i = menage.members.foyer_fiscal('prelevement_forfaitaire_unique_ir_annee_paiement', period)
                prelevement_forfaitaire_unique_ir_annee_paiement = menage.sum(prelevement_forfaitaire_unique_ir_annee_paiement_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                # On comptabilise ir_pv_immo ici directement, et non pas dans la variable 'irpp', car administrativement, cet impôt n'est pas dans l'irpp, et n'est déclaré dans le formulaire 2042C que pour calculer le revenu fiscal de référence. On colle à la définition administrative, afin d'avoir une variable 'irpp' qui soit comparable à l'IR du simulateur en ligne de la DGFiP
                # On prend en compte l'IR sur PV immobilières des foyers fiscaux dont le déclarant principal est dans le ménage
                ir_pv_immo_annee_paiement_i = menage.members.foyer_fiscal('ir_pv_immo_annee_paiement', period)
                ir_pv_immo_annee_paiement = menage.sum(ir_pv_immo_annee_paiement_i, role = FoyerFiscal.DECLARANT_PRINCIPAL)

                return irpp_economique_annee_paiement + prelevement_forfaitaire_liberatoire_annee_paiement + prelevement_forfaitaire_unique_ir_annee_paiement + ir_pv_immo_annee_paiement

        class impots_directs_menage_par_uc_annee_paiement(Variable):
            value_type = float
            entity = Menage
            definition_period = YEAR

            def formula(menage, period, parameters):
                impots_directs = menage('impots_directs_menage_annee_paiement', period)
                uc = menage('unites_consommation', period)
                return impots_directs / uc


        new_variables = [
            centile_niveau_de_vie_annee_paiement,
            impots_directs_menage_annee_paiement,
            impots_directs_menage_par_uc_annee_paiement,
            ir_brut_menage_annee_paiement,
            ir_brut_menage_par_uc_annee_paiement,
            ir_plaf_qf_menage_annee_paiement,
            ip_net_menage_annee_paiement,
            ip_net_menage_par_uc_annee_paiement,
            iaidrdi_menage_annee_paiement,
            iai_menage_annee_paiement,
            irpp_menage_annee_paiement,
            irpp_menage_par_uc_annee_paiement,
            irpp_economique_menage_annee_paiement,
            irpp_economique_menage_par_uc_annee_paiement,
            niveau_de_vie_annee_paiement,
            revenu_disponible_menage_annee_paiement,
            revenus_nets_foyers,
            impots_directs_annee_paiement,
            irpp_economique_annee_paiement,
            prelevement_forfaitaire_liberatoire_annee_paiement,
            prelevement_forfaitaire_unique_ir_annee_paiement,
            ir_pv_immo_annee_paiement,
            salaire_de_base_foyers,
            revenus_nets_f,
            ]

        for variable in new_variables:
            self.add_variable(variable)



########

tax_benefit_system = create_salaire_imposable_foyers(tax_benefit_system)
tax_benefit_system = tax_benefit_system_reform(tax_benefit_system)

####

survey_scenario = get_survey_scenario(
    tax_benefit_system = tax_benefit_system,
    year = 2018,
    varying_variable = "rni",
    use_marginal_tax_rate = True,
    rebuild_input_data = False
    )

##Console
period = 2018
year = 2018
varying_variable = "rni"
x = "rni_menage"
y = "revenu_disponible_menage_annee_paiement"
xtile = "centile"
nquantiles = 20


quantile = create_quantile(
        x = varying_variable,
        nquantiles = nquantiles,
        entity_name = FoyerFiscal,
        weight_variable = survey_scenario.weight_variable_by_entity[
            survey_scenario.tax_benefit_system.variables[varying_variable].entity.key]
        )
survey_scenario.tax_benefit_system.replace_variable(quantile)




## Creates the dataframe with the marginal tax rates as well as other variables

def create_data_frame_with_marginal_tax_rate(
        survey_scenario,
        target_variables = ["irpp", 'revenus_nets_foyers'],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus', 'revenus_nets_foyers'],
        year = year,
        ):

    assert year is not None

    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)

    df = survey_scenario.create_data_frame_by_entity(variables = variables + [varying])['foyer_fiscal']
    for target_variable in target_variables:
        df['taux_marginal_{}'.format(target_variable)] = pd.DataFrame({
            varying_variable: survey_scenario.compute_marginal_tax_rate(target_variable, period = year),
            })
    print(df)#on regarde la composition du dataframe
    df.to_csv('/Users/pablorodriguez/Desktop/try.csv', sep = ';')

    return df


## Plots boxplot for the emtr with outliers

def plot_marginal_tax_rate_by_quantile_box(
        survey_scenario,
        target_variable = 'revenus_nets_foyers',
        year = year,
        ):

    assert year is not None

    df = create_data_frame_with_marginal_tax_rate(
        survey_scenario = survey_scenario,
        target_variables = [target_variable],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus', "revenus_nets_foyers"],
        year = year,
        )
    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)


    # On exclut les foyers dont la variable sous-jacente est nulle (taux marginal non défini)
    df = df.loc[df[varying] > 0]
    ax = df.boxplot(by='quantile', 
                       column=['taux_marginal_revenus_nets_foyers'], 
                       grid=False,
                       sym="+",
                       )
    ax.set_title('')
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.set_xlabel("Net Taxable Income Vigintiles")
    ax.set_ylabel("Effective Marginal Tax Rate")
    rates_figure = ax.get_figure()

    return df, rates_figure

plot_marginal_tax_rate_by_quantile_box(survey_scenario=survey_scenario)

## Plots boxplot for the emtr without outliers

def plot_marginal_tax_rate_by_quantile_box_1(
        survey_scenario,
        target_variable = 'revenus_nets_foyers',
        year = year,
        ):

    assert year is not None

    df = create_data_frame_with_marginal_tax_rate(
        survey_scenario = survey_scenario,
        target_variables = [target_variable],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus', 'revenus_nets_foyers'],
        year = year,
        )
    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)


    # On exclut les foyers dont la variable sous-jacente est nulle (taux marginal non défini)
    df = df.loc[df[varying] > 0]
    ax = df.boxplot(by='quantile', 
                       column=['taux_marginal_revenus_nets_foyers'], 
                       grid=False,
                       sym="+",
                       showfliers=False,
                       )
    ax.set_title('')
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.set_xlabel("Net Taxable Income Vigintiles")
    ax.set_ylabel("Effective Marginal Tax Rate")
    rates_figure = ax.get_figure()

    return df, rates_figure

plot_marginal_tax_rate_by_quantile_box_1(survey_scenario=survey_scenario)


## Plots P10, P25, P50, P75, P95 of the mean marginal tax rate by quantile
#Emulates Fourcot, Rioux and Sicsic (2017)
# I did not consrve it because it does not seem readable

def plot_marginal_tax_rate_by_quantile(
        survey_scenario,
        target_variable = 'revenus_nets_foyers',
        year = year,
        ):

    assert year is not None

    df = create_data_frame_with_marginal_tax_rate(
        survey_scenario = survey_scenario,
        target_variables = [target_variable],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus'],
        year = year,
        )
    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)

    # On exclut les foyers dont la variable sous-jacente est nulle (taux marginal non défini)
    df = df.loc[df[varying] > 0]
    calc = wc.Calculator("weight_foyers")
    pop_by_quantile = df.groupby('quantile')
    df = pd.DataFrame({
        "taux_marginal_mean": calc.mean(pop_by_quantile, "taux_marginal_{}".format(target_variable)),
        "taux_marginal_min": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.0),
        "taux_marginal_p10": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.10),
        "taux_marginal_p25": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.25),
        "taux_marginal_median": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.50),
        "taux_marginal_p75": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.75),
        "taux_marginal_p95": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.95),
        "taux_marginal_p99": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.99),
        })
    ax = None
    for y_variable in ["taux_marginal_p10",
                       'taux_marginal_p25', 
                       "taux_marginal_median",
                       'taux_marginal_p75', 
                       'taux_marginal_p95']:
        ax = df.plot(
            ax = ax,
            y = y_variable,
            kind = 'line',
            )
    ax.set_title('')
    ax.legend().remove()
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.set_xlabel("Net Taxable Income Vigintiles")
    ax.set_ylabel("Income Tax Effective Marginal Tax Rate")
    rates_figure = ax.get_figure()

    return df, rates_figure

plot_marginal_tax_rate_by_quantile(survey_scenario=survey_scenario)


##Plots the mean marginal tax rate by quantile, as well as scatter plots of the effective marginal
#tax rates
#very interesting, allows to study heterogeneity by quantile

def plot_marginal_tax_rate_by_quantile_1(
        survey_scenario,
        target_variable = 'revenus_nets_foyers',
        year = year,
        ):

    assert year is not None

    df = create_data_frame_with_marginal_tax_rate(
        survey_scenario = survey_scenario,
        target_variables = [target_variable],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus', 'revenus_nets_foyers'],
        year = year,
        )
    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)

    # On exclut les foyers dont la variable sous-jacente est nulle (taux marginal non défini)
    df = df.loc[df[varying] > 0]
    calc = wc.Calculator("weight_foyers")
    ax = None
    ax = sns.scatterplot(
        x="quantile", 
        y="taux_marginal_revenus_nets_foyers", 
        data=df,
        color="firebrick")
    pop_by_quantile = df.groupby('quantile')
    df = pd.DataFrame({
        "taux_marginal_mean": calc.mean(pop_by_quantile, "taux_marginal_{}".format(target_variable)),
        "taux_marginal_min": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.0),
        "taux_marginal_p25": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.25),
        "taux_marginal_median": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.50),
        "taux_marginal_p75": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.75),
        "taux_marginal_p95": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.95),
        "taux_marginal_p99": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.99),
        })
    ax = df.plot(
        ax = ax,
        y = 'taux_marginal_mean',
        linewidth=3,
        )
    ax.set_title('')
    ax.legend().remove()
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.set_xlabel("Net Taxable Income Vigintiles")
    ax.set_ylabel("Effective Marginal Tax Rate")
    rates_figure = ax.get_figure()

    return df, rates_figure

plot_marginal_tax_rate_by_quantile_1(survey_scenario=survey_scenario)


##Plots the mean marginal tax rate by quantile

def plot_marginal_tax_rate_by_quantile_2(
        survey_scenario,
        target_variable = 'revenus_nets_foyers',
        year = year,
        ):

    assert year is not None

    df = create_data_frame_with_marginal_tax_rate(
        survey_scenario = survey_scenario,
        target_variables = [target_variable],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus', 'revenus_nets_foyers'],
        year = year,
        )
    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)

    # On exclut les foyers dont la variable sous-jacente est nulle (taux marginal non défini)
    df = df.loc[df[varying] > 0]
    calc = wc.Calculator("weight_foyers")
    pop_by_quantile = df.groupby('quantile')
    df = pd.DataFrame({
        "taux_marginal_mean": calc.mean(pop_by_quantile, "taux_marginal_{}".format(target_variable)),
        "taux_marginal_min": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.0),
        "taux_marginal_p25": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.25),
        "taux_marginal_median": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.50),
        "taux_marginal_p75": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.75),
        "taux_marginal_p95": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.95),
        "taux_marginal_p99": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.99),
        })
    ax = None
    ax = df.plot(
        ax = ax,
        y = 'taux_marginal_mean',
        )
    ax.set_title('')
    ax.legend().remove()
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.set_xlabel("Net Taxable Income Vigintiles")
    ax.set_ylabel("Effective Marginal Tax Rate")
    rates_figure = ax.get_figure()

    return df, rates_figure

plot_marginal_tax_rate_by_quantile_2(survey_scenario=survey_scenario)


##Plots kernel density of the emtr

def plot_marginal_tax_rate_by_kernel(
        survey_scenario,
        target_variable = 'revenus_nets_foyers',
        year = year,
        ):

    assert year is not None

    df = create_data_frame_with_marginal_tax_rate(
        survey_scenario = survey_scenario,
        target_variables = [target_variable],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus', 'revenus_nets_foyers'],
        year = year,
        )
    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)

    # On exclut les foyers dont la variable sous-jacente est nulle (taux marginal non défini)
    df = df.loc[df[varying] > 0]
    calc = wc.Calculator("weight_foyers")
    ax = None
    ax = sns.kdeplot( 
        x="taux_marginal_revenus_nets_foyers", 
        data=df
        )
    ax.set_title('')
    ax.legend().remove()
#    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.set_xlabel("Effective Marginal Tax Rate")
#    ax.set_ylabel("Income Tax Effective Marginal Tax Rate")
    rates_figure = ax.get_figure()

    return df, rates_figure

plot_marginal_tax_rate_by_kernel(survey_scenario=survey_scenario)
