*stop! //avoid running everything by mistake

*********************************************
* Merge with test case MTR and test function*
*********************************************


*********************************************
* Big Draft*
*********************************************

*if 	"`c(username)'" == "pablorodriguez" {
*	global path "/Users/pablorodriguez/codes_stage_crest"
*	
*}
*cd "${path}"


global path "/Users/pablorodriguez/Desktop/codes_stage_crest"
cd "${path}"


*global year 2016
global year 2019
*global year 2018
global household_composition adult2_child1
global case_filename norsa
*global social_contributions nosc
global social_contributions sc
global varying salaire_de_base
// salaire_de_base or salaire_imposable

if "$household_composition" == "adult1_child1" {
	global condition_sample nb_pac == 1 & nb_adult == 1 
	global subtitle_note "One adult and one child"
}
else if "$household_composition"  == "adult1_child0" {
	global condition_sample nb_pac == 0 & nb_adult == 1 
	global subtitle_note "One adult and no child"
}
else if "$household_composition" == "adult2_child1" {
	global condition_sample nb_pac == 1 & nb_adult == 2 
	global subtitle_note "Two adults and 1 child"
}
else if "$household_composition" == "adult2_child0" {
	global condition_sample nb_pac == 0 & nb_adult == 2 
	global subtitle_note "Two adults and no child"
}
else if "$household_composition" == "adult1_child2" {
	global condition_sample nb_pac == 2 & nb_adult == 1
	global subtitle_note "One adult and two children"
}
else if "$household_composition" == "adult1_child3" {
	global condition_sample nb_pac == 3 & nb_adult == 1
	global subtitle_note "One adult and three children"
}
else if "$household_composition" == "adult2_child2" {
	global condition_sample nb_pac == 2 & nb_adult == 2
	global subtitle_note "Two adult and two children"
}
else if "$household_composition" == "adult2_child3" {
	global condition_sample nb_pac == 2 & nb_adult == 1
	global subtitle_note "Two adults and three children"
}

if "${case_filename}" == "norsa" {
	global case_note "Removing RSA"
}
else if "${case_filename}" == "noppa" {
	global case_note "Removing PPA"
}
else if "${case_filename}" == "norsa_noppa" {
	global case_note "Removing RSA & PPA"
}
else if "${case_filename}" == "norsa_noppa_nodecote" {
	global case_note "Removing RSA, PPA & décote"
}

* Import tax levels from test cases and save as dta
insheet using "${path}/data_output_test_cases/varying_${varying}_taxlevels_${household_composition}_${year}_${case_filename}_${social_contributions}.csv", clear delimiter(;)
drop v1
rename revenubrutdutravail salaire_de_base // this should be adapted to the varying variable
rename revenudisponible revenu_disponible_testcase
gen simulated_test_case = 1
tempfile test_case_levels
save `test_case_levels'

* Import tax levels from test cases and save as dta for reform
insheet using "${path}/data_output_test_cases/varying_${varying}_taxlevels_ref_${household_composition}_${year}_${case_filename}_${social_contributions}.csv", clear delimiter(;)
drop v1
rename revenubrutdutravail salaire_de_base 
rename revenudisponible revenu_disponible_testcase_ref
gen simulated_test_case = 1
tempfile test_case_levels_ref
save `test_case_levels_ref'


* Import MTR and save as dta
insheet using "${path}/data_output_test_cases/varying_${varying}_emtr_${household_composition}_${year}_${case_filename}_${social_contributions}.csv", clear delimiter(;)
drop v1
gen simulated_test_case_MTR = 1
tempfile test_case_MTR
save `test_case_MTR'


* Import whole database with computed densities
if "$year" == "2019" {
use "${path}/EMTR_study_withdensities_2018.dta", clear
	* choose sample of interest
	keep if ${condition_sample} 
}
else use "${path}/EMTR_study_withdensities_${year}.dta", clear
	* choose sample of interest
	keep if ${condition_sample} 
	
* Add MTR and interpolate 
append using `test_case_MTR'
ipolate baseline_marginal_tax_rate salaire_de_base, gen(ipol_MTR_test_case_baseline)
ipolate reform_marginal_tax_rate salaire_de_base, gen(ipol_MTR_test_case_reformed)
drop if simulated_test_case_MTR == 1
lab var ipol_MTR_test_case_baseline "MTR"
lab var ipol_MTR_test_case_reformed "MTR (${case_note})"
replace ipol_MTR_test_case_baseline = ipol_MTR_test_case_baseline * 100
replace ipol_MTR_test_case_reformed = ipol_MTR_test_case_reformed * 100

* Same for levels
append using `test_case_levels'
ipolate revenu_disponible_testcase salaire_de_base, gen(ipol_revenu_disponible_testcase)
//ipolate same for reform
drop if simulated_test_case == 1
drop simulated_test_case
lab var ipol_revenu_disponible_testcase "Disposable Income"

* Same for levels with reform
append using `test_case_levels_ref'
ipolate revenu_disponible_testcase_ref salaire_de_base, gen(ipol_revenu_dispo_testcase_ref)
//ipolate same for reform
drop if simulated_test_case == 1
drop simulated_test_case
lab var ipol_revenu_dispo_testcase_ref "Disposable Income (${case_note})"

ta nb_pac
ta nb_adult
sleep 500

// a few checks
sum salaire_de_base if ipol_MTR_test_case_baseline == ., d
sum salaire_de_base if ipol_MTR_test_case_baseline != ., d
sum rni if ipol_MTR_test_case_baseline != ., d

/* If need to compute again CDF/PDF

			* New PDF/CDF with 0 excluded
			gen income_equal_to_0 = (salaire_de_base == 0)
			ta income_equal_to_0
			sum income_equal_to_0, d
			global share_0 = r(mean)
			di "$share_0"

			preserve
				tempfile pdf_nozeros
				keep if income_equal_to_0 == 0 // keep only positive incomes
				cap drop at
				cap drop cdf1 pdf1
				cap drop _merge
				range at 0 100000 1000 // 10000 20000    
				akdensity salaire_de_base, at(at) cdf(cdf1) gen(pdf1), [aw=weight_foyers]
				keep at cdf1 pdf1
				gen est = 1
				rename at salaire_de_base
				drop if mi(salaire_de_base)
				save `pdf_nozeros', replace 
			restore
				append using `pdf_nozeros' 
				sort salaire_de_base
				ipolate cdf1 salaire_de_base, gen(CDF_nozeros)
				ipolate pdf1 salaire_de_base, gen(PDF_nozeros)
				lab var CDF_nozeros "CDF"
				lab var PDF_nozeros "PDF"
				drop if est == 1
				drop cdf1 pdf1 est

			sum CDF_nozeros, d
			sum PDF_nozeros, d
			sum salaire_de_base if PDF_nozeros == ., d // non defined points with very high wages

			** Correction for zeros
			sum PDF_nozeros if salaire_de_base == 0, d
			sum CDF_nozeros if salaire_de_base == 0, d
			replace CDF_nozeros = (1 - $share_0) * CDF_nozeros + $share_0
			replace PDF_nozeros = (1 - $share_0) * PDF_nozeros
*/

* Graph according to RNI is not very useful
		/*sort rni
		#delim ;
		twoway (scatter ipol_MTR_test_case_baseline rni if rni < 100000, 
					yaxis(1) msize(small))
			(scatter ipol_MTR_test_case_reformed rni if rni < 100000, 
					yaxis(1) msize(small))
			(connected CDFipolate_${household_composition} rni if rni < 100000, 
				yaxis(2) yscale(alt axis(2)) mcolor(gs7) lcolor(gs7) msize(tiny))
			(connected PDFipolate_${household_composition} rni if rni < 100000, 
				yaxis(3) yscale(off axis(3)) mcolor(gs11) lcolor(gs11) msize(tiny)),
		name(adult1_kid0, replace)
		plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
		graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
		title("EMTR, ${year} taxes and data" "${subtitle_note}")
		note("MTR is simulated from test case, varying variable is 'salaire_de_base' .");
		graph export "${path}/graphs/EMTR/rates_over_rni_${household_composition}_${year}_${case_filename}_${social_contributions}.pdf", replace;
		#delim cr */

* Graph according to salaire_de_base is much more useful
sort salaire_de_base
#delim ;
twoway (scatter ipol_MTR_test_case_baseline salaire_de_base if salaire_de_base < 100000, 
			yaxis(1) msize(small))
	(scatter ipol_MTR_test_case_reformed salaire_de_base if salaire_de_base < 100000, 
			yaxis(1) msize(small))
	(connected CDFipol_sdb_${household_composition} salaire_de_base if salaire_de_base < 100000, 
		yaxis(2) yscale(alt axis(2)) mcolor(gs7) lcolor(gs7) msize(tiny))
	(connected PDFipol_sdb_${household_composition} salaire_de_base if salaire_de_base < 100000, 
		yaxis(3) yscale(off axis(3)) mcolor(gs11) lcolor(gs11) msize(tiny)),
name(full_scale, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("EMTR, ${year} taxes and data" "${subtitle_note}")
note("MTR is simulated from test case, varying variable is 'salaire_de_base' .");
*graph export "${path}/graphs/rates_over_sdb_${household_composition}_${year}_${case_filename}_${social_contributions}.pdf", replace;
#delim cr


/*#delim ;
twoway (scatter ipol_MTR_test_case_baseline salaire_de_base if salaire_de_base < 100000, 
			yaxis(1) msize(small))
	(scatter ipol_MTR_test_case_reformed salaire_de_base if salaire_de_base < 100000, 
			yaxis(1) msize(small))
	(connected CDF_nozeros salaire_de_base if salaire_de_base < 100000, 
		yaxis(2) yscale(alt axis(2)) mcolor(gs7) lcolor(gs7) msize(tiny))
	(connected PDF_nozeros salaire_de_base if salaire_de_base < 100000, 
		yaxis(3) yscale(off axis(3)) mcolor(gs11) lcolor(gs11) msize(tiny)),
name(full_scale, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("EMTR, ${year} taxes and data" "${subtitle_note}")
note("MTR is simulated from test case, varying variable is 'salaire_de_base' .");
graph export "${path}/graphs/EMTR/rates_over_sdb_${household_composition}_${year}_${case_filename}_${social_contributions}.pdf", replace;
#delim cr
*/

#delim ;
twoway (scatter ipol_MTR_test_case_baseline salaire_de_base if salaire_de_base < 30000, 
			yaxis(1) msize(small))
	(scatter ipol_MTR_test_case_reformed salaire_de_base if salaire_de_base < 30000, 
			yaxis(1) msize(small))
	(connected CDFipol_sdb_${household_composition} salaire_de_base if salaire_de_base < 30000, 
		yaxis(2) yscale(alt axis(2)) mcolor(gs7) lcolor(gs7) msize(tiny))
	(connected PDFipol_sdb_${household_composition} salaire_de_base if salaire_de_base < 30000, 
		yaxis(3) yscale(off axis(3)) mcolor(gs11) lcolor(gs11) msize(tiny)),
name(zoom, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
title("EMTR, ${year} taxes and data" "${subtitle_note}")
note("MTR is simulated from test case, varying variable is 'salaire_de_base' .");
*graph export "${path}/graphs/rates_over_lowsdb_${household_composition}_${year}_${case_filename}_${social_contributions}.png", replace;
#delim cr


*Plot income density

#delim ;
twoway (scatter PDFipol_sdb_${household_composition} salaire_de_base if salaire_de_base < 100000, 
		yaxis(1) msize(tiny)),
name(zoom, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*title("Income Density, ${year} " "${subtitle_note}");
graph export "${path}/graphs/density_${household_composition}_${year}_${case_filename}_${social_contributions}.png", replace;
#delim cr


/*
cap drop salaire_rounded
gen salaire_rounded = round(rni, 500)
cap drop _merge
cap drop rounded_emtr_adult1_child0
tempfile sdb_rounded_adult1_child0
preserve
keep if nb_pac == 0 & nb_adult == 1 & salaire_rounded < 32000
collapse mtr_irpp, by(salaire_rounded)
rename mtr_irpp rounded_mtr_adult1_child0
save `sdb_rounded_adult1_child0'
restore
merge m:1 salaire_rounded using `sdb_rounded_adult1_child0'
*/

sum rsa, d
sum irpp, d
* In TAXIPP taxes are negative and transfers are positive, we want the contrary
// old : gen TP = - irpp - rsa - ppe
* we want TP = tax on participation = T(y) - T(0)
* T(y) = sum(salaire de base) - disposable_income (beware ! disposable income defined at menage level)
* T(0) = ? let's take RSA level supposing full take up 

sum revenu_disponible_ff, d
sum ipol_revenu_disponible_testcase, d

/* This was selecting by hand the minimum transfer c0 in revenu_disponible_ff
scatter revenu_disponible_ff salaire_de_base if salaire_de_base < 50000 & revenu_disponible_ff < 50000
hist revenu_disponible_ff if revenu_disponible_ff < 30000
hist revenu_disponible_ff if revenu_disponible_ff < 7000
hist revenu_disponible_ff if revenu_disponible_ff > 6000 & revenu_disponible_ff < 6500
sum revenu_disponible_ff if revenu_disponible_ff > 6000 & revenu_disponible_ff < 6500, d
global c0 = r(p50)
*/

* We take the minimum from the test case
sum ipol_revenu_disponible_testcase, d
global c0 = r(min)
di " ${c0} "

gen TP = salaire_de_base - ipol_revenu_disponible_testcase - ${c0}
sum TP, d
// negative on average 
scatter TP salaire_de_base

gen TP_ref = salaire_de_base - ipol_revenu_dispo_testcase_ref - ${c0}

rename ipol_MTR_test_case_baseline Tprime
rename ipol_MTR_test_case_reformed Tprimeref
rename salaire_de_base y 

rename PDFipol_sdb_${household_composition} fy
rename CDFipol_sdb_${household_composition} Fy
//rename PDF_nozeros fy
//rename CDF_nozeros Fy

* We need Tprime between 0 and 1
replace Tprime = Tprime / 100
replace Tprimeref = Tprimeref / 100


***0. benchmark

cap drop R1 R2 R3
foreach s of numlist 1/3 {

if `s' == 1 {
	gen pi = 0
}
else if `s' == 2 {
	gen pi = 0.4
}
else if `s' == 3 {
	gen pi = .
	replace pi = 0.4 - 0.2 * (y/30000)^(1/2) if y <= 30000
	replace pi = 0.2 if y > 30000
}

scalar epsi = 0.33

*g1
gen g1 = 1 - Fy
label var g1 "g1 = 1 - F"

***with transfers

*g2
gen g2 = (epsi)*y*(fy)*(Tprime/(1-Tprime))
label var g2 "g2 = epsilon*y*f*T'/(1-T'), benchmark"
gen g2reform = (epsi)*y*(fy)*(Tprimeref/(1-Tprimeref))
label var g2reform "g2 = epsilon*y*f*T'/(1-T'), reform"

*g3
gen gtilde3 = (fy)*pi*(TP/(y-TP))
label var gtilde3 "f*pi*T/(y-T), benchmark"
integ gtilde3 y, gen(intg3) trapezoid

gen g3 = r(integral)  - intg3
label var g3 "g3 = integral of f * pi * T/(y-T), benchmark"
drop intg3 gtilde3

gen gtilde3 = (fy)*pi*(TP_ref/(y-TP_ref))
label var gtilde3 "f*pi*T/(y-T), benchmark"
integ gtilde3 y, gen(intg3) trapezoid

gen g3reform = r(integral)  - intg3
label var g3reform "g3reform = integral of f * pi * Tref/(y-Tref), benchmark"
drop intg3 gtilde3

*revenue effect
gen R`s' = g1 - g2 - g3
gen R`s'_reform = g1 - g2reform - g3reform

if `s' == 1 {
	lab var R`s' "Ext. margin = 0"
	lab var R`s'_reform "Ext. margin = 0 (${case_note})"
}
else if `s' == 2 {
	lab var R`s' "Ext. margin = 0.4"
	lab var R`s'_reform "Ext. margin = 0.4 (${case_note})"
}
else if `s' == 3 {
	lab var R`s' "Ext. margin decreasing "
	lab var R`s'_reform "Ext. margin decreasing (${case_note})"
}
drop g1 g2 g2reform g3 g3reform pi 

}

if "$household_composition" == "adult1_child1" {
#delim ;
twoway (scatter R1 y if y < 45000, msize(vsmall))
(scatter R2 y if y < 45000, msize(vsmall))
(scatter R3 y if y < 45000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(29940, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*note("MTR is simulated from test case, varying variable is '${varying}' .");
graph export "${path}/graphs/rtl_${household_composition}_${year}_${case_filename}_various_pi_${social_contributions}.png", replace;
#delim cr



#delim ;
twoway (scatter R3 y if y < 45000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(29940, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y_final, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
ytitle("")
legend(off)
title("");
*note("MTR is simulated from test case, varying variable is '${varying}' .");
graph export "${path}/graphs/rtl_${household_composition}_${year}_${case_filename}_${social_contributions}.png", replace;
#delim cr


#delim ;
twoway (scatter R1 y if y > 16000 & y < 20000, msize(vsmall))
(scatter R2 y if y > 16000 & y < 20000, msize(vsmall))
(scatter R3 y if y > 16000 & y < 20000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(29940, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y_fullscale, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*title("Test function (R_tl), ${year}" "${subtitle_note}")
*note("MTR is simulated from test case, varying variable is '${varying}' .");
#delim cr

#delim ;
twoway (scatter R3 y if y < 35000, msize(vsmall))
(scatter R3_reform y if y < 35000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(29940, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y_ref, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*note("MTR is simulated from test case, varying variable is '${varying}' .");
graph export "${path}/graphs/rtl_dec_${household_composition}_${year}_${case_filename}_${social_contributions}.png", replace;
#delim cr
// test with higher elasticity, no export:
#delim ; 
twoway (scatter R2 y if y < 35000, msize(vsmall))
(scatter R2_reform y if y < 35000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(29940, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y_ref, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*title("Test function (R_tl), ${year}" "${subtitle_note}")
*note("MTR is simulated from test case, varying variable is '${varying}' .");
#delim cr
}
else if "$household_composition"  == "adult1_child0" {
#delim ;
twoway (scatter R1 y if y < 45000, msize(vsmall))
(scatter R2 y if y < 45000, msize(vsmall))
(scatter R3 y if y < 45000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(27381, lcol(black) lpattern(dash))
xline(7301, lcol(red) lpattern(dash))
name(R_y, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*note("MTR is simulated from test case, varying variable is '${varying}' .");
graph export "${path}/graphs/rtl_${household_composition}_${year}_${case_filename}_various_pi_${social_contributions}.png", replace;
#delim cr



#delim ;
twoway (scatter R3 y if y < 45000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(27381, lcol(black) lpattern(dash))
xline(7301, lcol(red) lpattern(dash))
name(R_y_final, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
ytitle("")
legend(off)
title("");
*note("MTR is simulated from test case, varying variable is '${varying}' .");
graph export "${path}/graphs/rtl_${household_composition}_${year}_${case_filename}_${social_contributions}.png", replace;
#delim cr


#delim ;
twoway (scatter R1 y if y > 16000 & y < 20000, msize(vsmall))
(scatter R2 y if y > 16000 & y < 20000, msize(vsmall))
(scatter R3 y if y > 16000 & y < 20000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(27381, lcol(black) lpattern(dash))
xline(7301, lcol(red) lpattern(dash))
name(R_y_fullscale, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*title("Test function (R_tl), ${year}" "${subtitle_note}")
*note("MTR is simulated from test case, varying variable is '${varying}' .");
#delim cr

#delim ;
twoway (scatter R3 y if y < 35000, msize(vsmall))
(scatter R3_reform y if y < 35000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(27381, lcol(black) lpattern(dash))
xline(7301, lcol(red) lpattern(dash))
name(R_y_ref, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*note("MTR is simulated from test case, varying variable is '${varying}' .");
graph export "${path}/graphs/rtl_dec_${household_composition}_${year}_${case_filename}_${social_contributions}.png", replace;
#delim cr
// test with higher elasticity, no export:
#delim ; 
twoway (scatter R2 y if y < 35000, msize(vsmall))
(scatter R2_reform y if y < 35000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(27381, lcol(black) lpattern(dash))
xline(7301, lcol(red) lpattern(dash))
name(R_y_ref, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*title("Test function (R_tl), ${year}" "${subtitle_note}")
*note("MTR is simulated from test case, varying variable is '${varying}' .");
#delim cr
}
else if "$household_composition" == "adult2_child1" {
#delim ;
twoway (scatter R1 y if y < 45000, msize(vsmall))
(scatter R2 y if y < 45000, msize(vsmall))
(scatter R3 y if y < 45000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(35028, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*note("MTR is simulated from test case, varying variable is '${varying}' .");
graph export "${path}/graphs/rtl_${household_composition}_${year}_${case_filename}_various_pi_${social_contributions}.png", replace;
#delim cr



#delim ;
twoway (scatter R3 y if y < 45000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(35028, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y_final, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
ytitle("")
legend(off)
title("");
*note("MTR is simulated from test case, varying variable is '${varying}' .");
graph export "${path}/graphs/rtl_${household_composition}_${year}_${case_filename}_${social_contributions}.png", replace;
#delim cr


#delim ;
twoway (scatter R1 y if y > 16000 & y < 20000, msize(vsmall))
(scatter R2 y if y > 16000 & y < 20000, msize(vsmall))
(scatter R3 y if y > 16000 & y < 20000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(35028, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y_fullscale, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*title("Test function (R_tl), ${year}" "${subtitle_note}")
*note("MTR is simulated from test case, varying variable is '${varying}' .");
#delim cr

#delim ;
twoway (scatter R3 y if y < 35000, msize(vsmall))
(scatter R3_reform y if y < 35000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(35028, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y_ref, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*note("MTR is simulated from test case, varying variable is '${varying}' .");
graph export "${path}/graphs/rtl_dec_${household_composition}_${year}_${case_filename}_${social_contributions}.png", replace;
#delim cr
// test with higher elasticity, no export:
#delim ; 
twoway (scatter R2 y if y < 35000, msize(vsmall))
(scatter R2_reform y if y < 35000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(35028, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y_ref, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*title("Test function (R_tl), ${year}" "${subtitle_note}")
*note("MTR is simulated from test case, varying variable is '${varying}' .");
#delim cr
}
else if "$household_composition" == "adult2_child0" {
#delim ;
twoway (scatter R1 y if y < 45000, msize(vsmall))
(scatter R2 y if y < 45000, msize(vsmall))
(scatter R3 y if y < 45000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(29940, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*note("MTR is simulated from test case, varying variable is '${varying}' .");
graph export "${path}/graphs/rtl_${household_composition}_${year}_${case_filename}_various_pi_${social_contributions}.png", replace;
#delim cr



#delim ;
twoway (scatter R3 y if y < 45000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(29940, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y_final, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
ytitle("")
legend(off)
title("");
*note("MTR is simulated from test case, varying variable is '${varying}' .");
graph export "${path}/graphs/rtl_${household_composition}_${year}_${case_filename}_${social_contributions}.png", replace;
#delim cr


#delim ;
twoway (scatter R1 y if y > 16000 & y < 20000, msize(vsmall))
(scatter R2 y if y > 16000 & y < 20000, msize(vsmall))
(scatter R3 y if y > 16000 & y < 20000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(29940, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y_fullscale, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*title("Test function (R_tl), ${year}" "${subtitle_note}")
*note("MTR is simulated from test case, varying variable is '${varying}' .");
#delim cr

#delim ;
twoway (scatter R3 y if y < 35000, msize(vsmall))
(scatter R3_reform y if y < 35000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(29940, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y_ref, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*note("MTR is simulated from test case, varying variable is '${varying}' .");
graph export "${path}/graphs/rtl_dec_${household_composition}_${year}_${case_filename}_${social_contributions}.png", replace;
#delim cr
// test with higher elasticity, no export:
#delim ; 
twoway (scatter R2 y if y < 35000, msize(vsmall))
(scatter R2_reform y if y < 35000, msize(vsmall)),
xline(0, lcol(black) lpattern(dash))
xline(29940, lcol(black) lpattern(dash))
xline(9936, lcol(red) lpattern(dash))
name(R_y_ref, replace)
plotregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white)) 
graphregion(margin(l=5 r=5 t=2 b=2) fcolor(white) lstyle(none) lcolor(white))
xtitle("Gross wage")
legend(off)
title("");
*title("Test function (R_tl), ${year}" "${subtitle_note}")
*note("MTR is simulated from test case, varying variable is '${varying}' .");
#delim cr
}
