# -*- coding: utf-8 -*-
"""


@author: Pablo Rodriguez

Code computes effective marginal tax rates on tax and transfer system from actual 
French taxpayers
1) Reforms coding (allows to create some variables), apply them to TBS
2) Create dataframe with emtr
3) Plot boxplots
4) Plot emtr by quantiles
5) Plot kernel density of emtr

"""


#############################
# Very important remarks on this code 

# For an unknown reason when running the code, some graphs overlap with each other .
# In order to obtain the desired output, one must run the code first and then
# run individually the following lines, corresponding to the plotting functions:
# Line 200: boxplot with outliers
# Line 244: boxplot without outliers
# Line 310: Plots P10, P25, P50, P75, P95 of the mean marginal tax rate by quantile
# Line 375: plots the mean marginal tax rate by quantile, as well as scatter plots
# of the effective marginal tax rates
# Line 433: mean marginal tax rate by quantile
# Line 476: kernel density of the emtr

#############################

import copy
import pandas as pd
import numpy
import weightedcalcs as wc
import seaborn as sns


from taxipp.base import taxipp_tax_and_benefit_system
from openfisca_survey_manager.variables import create_quantile
from openfisca_france_data import france_data_tax_benefit_system
from openfisca_france_data.erfs_fpr.get_survey_scenario import get_survey_scenario
from openfisca_core.rates import average_rate
from openfisca_france.model.base import *
from numpy import around
from openfisca_core.reforms import Reform
from taxipp.utils.general_utils import to_percent_round_formatter
from taxipp.utils.general_utils import ipp_colors, label_by_variable




##Setting TBS
tax_benefit_system = taxipp_tax_and_benefit_system

class create_salaire_imposable_foyers(Reform):
    name = "Salaire imposable foyers"
    def apply(self):
        class salaire_imposable_foyers(Variable):
                value_type = float
                entity = FoyerFiscal
                label = u"Salaires imposables totaux du foyer"
                definition_period = YEAR

                def formula(foyer_fiscal, period, parameters):
                    salaire_imposable_i = foyer_fiscal.members('salaire_imposable', period, options = [ADD])

                    return foyer_fiscal.sum(salaire_imposable_i)
        self.add_variable(salaire_imposable_foyers)

class create_revenus_nets_foyers(Reform):
    name = "Revenus nets du foyer"
    def apply(self):
        class revenus_nets_foyers(Variable):
                value_type = float
                entity = FoyerFiscal
                label = u"Revenus nets du foyer"
                definition_period = YEAR

                def formula(foyer_fiscal, period, parameters):
                    pensions_nettes_i = foyer_fiscal.members('pensions_nettes', period)
                    revenus_nets_du_capital_i = foyer_fiscal.members('revenus_nets_du_capital', period)
                    revenus_nets_du_travail_i = foyer_fiscal.members('revenus_nets_du_travail', period)

                    revenus_nets = foyer_fiscal.sum(pensions_nettes_i + revenus_nets_du_capital_i + revenus_nets_du_travail_i)
                    return revenus_nets
        self.add_variable(revenus_nets_foyers)

########

tax_benefit_system = create_salaire_imposable_foyers(tax_benefit_system)
tax_benefit_system = create_revenus_nets_foyers(tax_benefit_system)


####

survey_scenario = get_survey_scenario(
    tax_benefit_system = tax_benefit_system,
    year = 2018,
    varying_variable = "rni",
    use_marginal_tax_rate = True,
    rebuild_input_data = False
    )

##Console
period = 2018
year = 2018
varying_variable = "rni"
x = "salaire_imposable"
y = "revenu_disponible"
xtile = "centile"
nquantiles = 20


quantile = create_quantile(
        x = varying_variable,
        nquantiles = nquantiles,
        entity_name = FoyerFiscal,
        weight_variable = survey_scenario.weight_variable_by_entity[
            survey_scenario.tax_benefit_system.variables[varying_variable].entity.key]
        )
survey_scenario.tax_benefit_system.replace_variable(quantile)




## Creates the dataframe with the marginal tax rates as well as other variables
#useful to check if some variables are not well computed

def create_data_frame_with_marginal_tax_rate(
        survey_scenario,
        target_variables = ["irpp"],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus'],
        year = year,
        ):

    assert year is not None

    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)

    df = survey_scenario.create_data_frame_by_entity(variables = variables + [varying])['foyer_fiscal']
    for target_variable in target_variables:
        df['taux_marginal_{}'.format(target_variable)] = pd.DataFrame({
            varying_variable: survey_scenario.compute_marginal_tax_rate(target_variable, period = year)-1,
            })

    return df


## Plots boxplot for the emtr with outliers

def plot_marginal_tax_rate_by_quantile_box(
        survey_scenario,
        target_variable = 'irpp',
        year = year,
        ):

    assert year is not None

    df = create_data_frame_with_marginal_tax_rate(
        survey_scenario = survey_scenario,
        target_variables = [target_variable],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus'],
        year = year,
        )
    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)


    # On exclut les foyers dont la variable sous-jacente est nulle (taux marginal non défini)
    df = df.loc[df[varying] > 0]
    ax = df.boxplot(by='quantile', 
                       column=['taux_marginal_irpp'], 
                       grid=False,
                       sym="+",
                       )
    ax.set_title('')
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.set_xlabel("Net Taxable Income Vigintiles")
    ax.set_ylabel("Income Tax Effective Marginal Tax Rate")
    rates_figure = ax.get_figure()

    return df, rates_figure

plot_marginal_tax_rate_by_quantile_box(survey_scenario=survey_scenario)

## Plots boxplot for the emtr without outliers

def plot_marginal_tax_rate_by_quantile_box_1(
        survey_scenario,
        target_variable = 'irpp',
        year = year,
        ):

    assert year is not None

    df = create_data_frame_with_marginal_tax_rate(
        survey_scenario = survey_scenario,
        target_variables = [target_variable],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus'],
        year = year,
        )
    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)


    # On exclut les foyers dont la variable sous-jacente est nulle (taux marginal non défini)
    df = df.loc[df[varying] > 0]
    ax = df.boxplot(by='quantile', 
                       column=['taux_marginal_irpp'], 
                       grid=False,
                       sym="+",
                       showfliers=False,
                       )
    ax.set_title('')
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.set_xlabel("Net Taxable Income Vigintiles")
    ax.set_ylabel("Income Tax Effective Marginal Tax Rate")
    rates_figure = ax.get_figure()

    return df, rates_figure

plot_marginal_tax_rate_by_quantile_box_1(survey_scenario=survey_scenario)




## Plots P10, P25, P50, P75, P95 of the mean marginaltax rate by quantile
#Emulates Fourcot, Rioux and Sicsic (2017) approach


def plot_marginal_tax_rate_by_quantile(
        survey_scenario,
        target_variable = 'irpp',
        year = year,
        ):

    assert year is not None

    df = create_data_frame_with_marginal_tax_rate(
        survey_scenario = survey_scenario,
        target_variables = [target_variable],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus'],
        year = year,
        )
    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)

    # On exclut les foyers dont la variable sous-jacente est nulle (taux marginal non défini)
    df = df.loc[df[varying] > 0]
    calc = wc.Calculator("weight_foyers")
    pop_by_quantile = df.groupby('quantile')
    df = pd.DataFrame({
        "taux_marginal_mean": calc.mean(pop_by_quantile, "taux_marginal_{}".format(target_variable)),
        "taux_marginal_min": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.0),
        "taux_marginal_p10": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.10),
        "taux_marginal_p25": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.25),
        "taux_marginal_median": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.50),
        "taux_marginal_p75": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.75),
        "taux_marginal_p95": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.95),
        "taux_marginal_p99": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.99),
        })
    ax = None
    for y_variable in ["taux_marginal_p10",
                       'taux_marginal_p25', 
                       "taux_marginal_median",
                       'taux_marginal_p75', 
                       'taux_marginal_p95']:
        ax = df.plot(
            ax = ax,
            y = y_variable,
            kind = 'line',
            )
    ax.set_title('')
    ax.legend().remove()
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.set_xlabel("Net Taxable Income Vigintiles")
    ax.set_ylabel("Income Tax Effective Marginal Tax Rate")
    rates_figure = ax.get_figure()

    return df, rates_figure

plot_marginal_tax_rate_by_quantile(survey_scenario=survey_scenario)



##Plots the mean marginal tax rate by quantile, as well as scatter plots of the effective marginal
#tax rates
#very interesting, allows to study heterogeneity by quantile

def plot_marginal_tax_rate_by_quantile_1(
        survey_scenario,
        target_variable = 'irpp',
        year = year,
        ):

    assert year is not None

    df = create_data_frame_with_marginal_tax_rate(
        survey_scenario = survey_scenario,
        target_variables = [target_variable],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus'],
        year = year,
        )
    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)

    # On exclut les foyers dont la variable sous-jacente est nulle (taux marginal non défini)
    df = df.loc[df[varying] > 0]
    calc = wc.Calculator("weight_foyers")
    ax = None
    ax = sns.scatterplot(
        x="quantile", 
        y="taux_marginal_irpp", 
        data=df,
        color="firebrick")
    pop_by_quantile = df.groupby('quantile')
    df = pd.DataFrame({
        "taux_marginal_mean": calc.mean(pop_by_quantile, "taux_marginal_{}".format(target_variable)),
        "taux_marginal_min": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.0),
        "taux_marginal_p25": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.25),
        "taux_marginal_median": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.50),
        "taux_marginal_p75": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.75),
        "taux_marginal_p95": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.95),
        "taux_marginal_p99": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.99),
        })
    ax = df.plot(
        ax = ax,
        y = 'taux_marginal_mean',
        linewidth=3,
        )
    ax.set_title('')
    ax.legend().remove()
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.set_xlabel("Net Taxable Income Vigintiles")
    ax.set_ylabel("Income Tax Effective Marginal Tax Rate")
    rates_figure = ax.get_figure()

    return df, rates_figure

plot_marginal_tax_rate_by_quantile_1(survey_scenario=survey_scenario)


##Plots the mean marginal tax rate by quantile


def plot_marginal_tax_rate_by_quantile_2(
        survey_scenario,
        target_variable = 'irpp',
        year = year,
        ):

    assert year is not None

    df = create_data_frame_with_marginal_tax_rate(
        survey_scenario = survey_scenario,
        target_variables = [target_variable],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus'],
        year = year,
        )
    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)

    # On exclut les foyers dont la variable sous-jacente est nulle (taux marginal non défini)
    df = df.loc[df[varying] > 0]
    calc = wc.Calculator("weight_foyers")
    pop_by_quantile = df.groupby('quantile')
    df = pd.DataFrame({
        "taux_marginal_mean": calc.mean(pop_by_quantile, "taux_marginal_{}".format(target_variable)),
        "taux_marginal_min": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.0),
        "taux_marginal_p25": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.25),
        "taux_marginal_median": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.50),
        "taux_marginal_p75": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.75),
        "taux_marginal_p95": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.95),
        "taux_marginal_p99": calc.quantile(pop_by_quantile,  "taux_marginal_{}".format(target_variable), 0.99),
        })
    ax = None
    ax = df.plot(
        ax = ax,
        y = 'taux_marginal_mean',
        )
    ax.set_title('')
    ax.legend().remove()
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.set_xlabel("Net Taxable Income Vigintiles")
    ax.set_ylabel("Income Tax Effective Marginal Tax Rate")
    rates_figure = ax.get_figure()

    return df, rates_figure


plot_marginal_tax_rate_by_quantile_2(survey_scenario=survey_scenario)


##Plots kernel density of the emtr

def plot_marginal_tax_rate_by_kernel(
        survey_scenario,
        target_variable = 'irpp',
        year = year,
        ):

    assert year is not None

    df = create_data_frame_with_marginal_tax_rate(
        survey_scenario = survey_scenario,
        target_variables = [target_variable],
        variables = [
            'quantile', 'taux_statutaire', 'weight_foyers',
            'rni', 'revenu_categoriel_capital', 'revenu_categoriel_tspr',
            'ir_brut', 'ir_plaf_qf', 'ip_net', 'decote_gain_fiscal', 'iai', 'irpp',
            'credits_impot', 'retraite_imposable',
            'reduction_ss_condition_revenus'],
        year = year,
        )
    varying_variable = survey_scenario.varying_variable
    varying = varying_variable
    if survey_scenario.tax_benefit_system.variables[varying].entity.key == 'individu':
        varying = '{}_foyers'.format(varying)

    # On exclut les foyers dont la variable sous-jacente est nulle (taux marginal non défini)
    df = df.loc[df[varying] > 0]
    ax = None
    ax = sns.kdeplot( 
        x="taux_marginal_irpp", 
        data=df
        )
    ax.set_title('')
    ax.legend().remove()
#    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.set_xlabel("Marginal Tax Rate on the Income Tax")
#    ax.set_ylabel("Income Tax Effective Marginal Tax Rate")
    rates_figure = ax.get_figure()

    return df, rates_figure

plot_marginal_tax_rate_by_kernel(survey_scenario=survey_scenario)
