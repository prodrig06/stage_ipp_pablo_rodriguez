# coding: utf-8

"""

@author: Pablo Rodriguez

Code computes marginal tax rates for 8 test cases
1) Reforms coding (allows to mute some taxes/benefits), apply them to two TBS
2) Plot effective marginal tax rates

"""


import copy
import logging
import numpy
import pandas as pd
import matplotlib.pyplot as plt


from numpy import logical_or as or_, logical_and as and_

from taxipp.base import taxipp_tax_and_benefit_system
# For our local version
from openfisca_france_data import france_data_tax_benefit_system
from openfisca_core import periods

# For reform
#from numpy import logical_and as and_, maximum as max_
from openfisca_france.model.base import *
from numpy import around
from openfisca_core.reforms import Reform

from openfisca_core import periods
from openfisca_core.reforms import Reform
from openfisca_france_data import france_data_tax_benefit_system
from openfisca_core.rates import marginal_rate, average_rate
from openfisca_france_data.smic import smic_annuel_net_by_year, smic_annuel_brut_by_year
from taxipp.utils.general_utils import to_percent_round_formatter
from taxipp.utils.test_case import base
from taxipp.utils.test_case.utils import (
    calculate,
    create_scenario_actif,
    create_scenario_rentier,
    create_scenario_chomeur,
    )


log = logging.getLogger(__name__)


#############################
# Remarks on this code from Elie

# Salaire_imposable is probably not what we want because progressivity could (in theory) 
# take place among contributions and social "cotisations"
# 

#############################

period = 2018
social_contributions_muted = True
varying = "salaire_de_base" # can be "salaire_de_base" or "salaire_imposable" 
x = 7 ## upper bound of graphs and tables in terms of SMIC

# Output path
import os
current_path = str(os.getcwd())
graphs_path = (os.path.join(current_path, "graphs/"))
data_output_path = (os.path.join(current_path, "data_output_test_cases/"))

# TBS initialization
##########################        

tax_benefit_system = france_data_tax_benefit_system



######################
class mute_decote(Reform):
    name = "Mute the decote mechanism in the computation of net taxes"
    def apply(self):
        class ip_net(Variable):
            value_type = float
            entity = FoyerFiscal
            label = "Impôt sur le revenu après décote et réduction sous condition de revenus, avant réductions"
            definition_period = YEAR
            def formula(foyer_fiscal, period, parameters):
                '''
                Impôt net avant réductions
                '''
                cncn_info_i = foyer_fiscal.members('cncn_info', period)
                decote = foyer_fiscal('decote', period)
                ir_plaf_qf = foyer_fiscal('ir_plaf_qf', period)
                taux = parameters(period).impot_revenu.rpns.taux16
                # N'est pas véritablement une 'réduction', cf. la définition de cette variable
                reduction_ss_condition_revenus = foyer_fiscal('reduction_ss_condition_revenus', period)
                # decote replaced by 0 below
                return around(max_(0, ir_plaf_qf + foyer_fiscal.sum(cncn_info_i) * taux - 0 - 0))
        class decote(Variable):
            value_type = float
            entity = FoyerFiscal
            label = "Decote set to 0"
            definition_period = YEAR
            def formula_2014_01_01(foyer_fiscal, period, parameters):
                return 0
            def formula_2001_01_01(foyer_fiscal, period, parameters):
                return 0
        class decote_gain_fiscal(Variable):
            value_type = float
            entity = FoyerFiscal
            label = "Decote gain fiscal set to 0"
            definition_period = YEAR
            def formula(foyer_fiscal, period, parameters):
                return 0
        self.replace_variable(ip_net)
        self.replace_variable(decote_gain_fiscal)
        self.replace_variable(decote)
        # nullyfying the decote variable should be enough
######################
class mute_ppa(Reform):
    name = "Mute the ppa"
    def apply(self):
        class ppa(Variable):
            value_type = float
            entity = Famille
            label = "Muted PA"
            definition_period = MONTH
            def formula_2016_01_01(famille, period, parameters):
                '''
                PA (fixee a 0)
                '''
                return 0
        self.replace_variable(ppa)
######################
class mute_rsa(Reform):
    name = "Mute the rsa"
    def apply(self):
        class rsa(Variable):
            value_type = float
            entity = Famille
            label = "Muted RSA"
            definition_period = MONTH
            def formula_2009_06_01(famille, period, parameters):
                '''
                RSA muted (set to 0)
                '''
                return 0
        self.replace_variable(rsa)
######################
class mute_reduc_exceptionelle(Reform):
    name = "Mute the reduction impot exceptionnelle Valls"
    def apply(self):
        class reduction_impot_exceptionnelle(Variable):
            value_type = float
            entity = FoyerFiscal
            label = "Muted reduction exceptionnelle"
            end = '2013-12-31'
            definition_period = YEAR
            def formula_2013_01_01(foyer_fiscal, period, parameters):
                '''
                Muted (set to 0)
                '''
                return 0
        self.replace_variable(reduction_impot_exceptionnelle)

######################
class mute_aides_logement(Reform):
    name = "Mute housing benefits"
    def apply(self):
        class aide_logement(Variable):
            value_type = float
            entity = Famille
            label = "Aide au logement tout type"
            definition_period = MONTH
            def formula(famille, period):
                '''
                Aide au logement
                '''
                apl = famille('apl', period)
                als = famille('als', period)
                alf = famille('alf', period)
                return 0
                #return max_(max_(apl, als), alf)
        self.replace_variable(aide_logement)
######################
class mute_social_contributions(Reform):
    name = "Remove CSG and CRDS"
    def apply(self):
        class csg_imposable_salaire(Variable):
            value_type = float
            entity = Individu
            label = "CSG imposable employee"
            definition_period = MONTH
            def formula(individu, period, parameters):
                '''
                muted formula
                '''
                return 0
        class csg_deductible_salaire(Variable):
            value_type = float
            entity = Individu
            label = "CSG deductible employee"
            definition_period = MONTH
            def formula(individu, period, parameters):
                '''
                muted formula
                '''
                return 0
        class crds_salaire(Variable):
            value_type = float
            entity = Individu
            label = "CRDS imposable employee"
            definition_period = MONTH
            def formula(individu, period, parameters):
                '''
                muted formula
                '''
                return 0
        class cotisations_salariales(Variable):
            value_type = float
            entity = Individu
            label = "social contributions on employees"
            definition_period = MONTH
            def formula(individu, period, parameters):
                '''
                muted formula
                '''
                return 0
            
        self.replace_variable(csg_imposable_salaire)
        self.replace_variable(csg_deductible_salaire)
        self.replace_variable(crds_salaire)
        self.replace_variable(cotisations_salariales)
######################
class create_agg_social_contributions(Reform):
    name = "Aggregate of all social contributions"
    def apply(self):
        class agg_social_contributions(Variable):
            value_type = float
            entity = Individu
            label = "Sum of CSG, CRDS and other social contributions"
            definition_period = MONTH
            def formula(individu, period, parameters):
                '''
                Simple sum
                '''
                cotisations_salariales = individu('cotisations_salariales', period)
                crds_salaire = individu('crds_salaire', period)
                csg_imposable_salaire = individu('csg_imposable_salaire', period)
                csg_deductible_salaire = individu('csg_deductible_salaire', period)                
                return cotisations_salariales + crds_salaire + csg_imposable_salaire + csg_deductible_salaire
        self.add_variable(agg_social_contributions)

######################
def null_decote(parameters):
    year = 2018
    reform_period = periods.period(str(year))
    # Reform
    parameters.impot_revenu.decote.taux.update(start = reform_period, value = 0)
    parameters.impot_revenu.decote.seuil_celib.update(start = reform_period, value = 0 )
    parameters.impot_revenu.decote.seuil_couple.update(start = reform_period, value = 0 )
    return parameters

class mute_decote_by_parameters(Reform):
    name = "explicit_title"
    def apply(self):
        self.modify_parameters(modifier_function = null_decote)
#######################

### Modifications for both systems: mute housing benefits and add one variable
# System without reform
tax_benefit_system = mute_aides_logement(tax_benefit_system)
#tax_benefit_system = create_agg_social_contributions(tax_benefit_system)


if social_contributions_muted == True:
    ### System without reform
    tax_benefit_system = mute_social_contributions(tax_benefit_system)



def create_test_case_dataframe(
        variables,
        input_variable = 'salaire_de_base',
        baseline_tax_benefit_system = None,
        tax_benefit_system = None,
        difference = False,
        graph_options = {},
        test_case_options = {},
        period = None,
        ):
    '''
    Produit les données d'un cas-type (individu, ménage, famille ou foyer dont les caractéristiques sont définis dans
    le dictionnaire test_case_options en argument) pour différent niveau d'une variable d'input.
    '''
    assert period is not None
    assert tax_benefit_system is not None
    assert not((difference is True) and (baseline_tax_benefit_system is None))

    # Création des scenarios kwargs définissant le cas-types
    year = int(str(periods.period(period).this_year))
    if (input_variable == "salaire_net" or input_variable == "salaire_de_base"):
        scenarios_kwargs = create_scenario_actif(
            year = year,
            test_case_options = test_case_options,
            graph_options = graph_options,
            )
    elif "chomage" in test_case_options:
        scenarios_kwargs = create_scenario_chomeur(
            year = year,
            test_case_options = test_case_options,
            graph_options = graph_options,
            )
    else:
        scenarios_kwargs = create_scenario_rentier(
            revenu = input_variable,
            year = year,
            test_case_options = test_case_options,
            graph_options = graph_options,
            )

    # Compute RSA first : necessary for RSA computation to be right (due to existing OF-Core bug)
    variables_to_calculate = copy.deepcopy(variables)
    if 'rsa' in variables:
        variables_to_calculate.remove('rsa')
        variables_to_calculate.insert(0, 'rsa')

    # Simulation du cas-types
    variables = set([input_variable] + variables_to_calculate)

    df = calculate(
        period = period,
        scenarios_kwargs = scenarios_kwargs,
        tax_benefit_system = tax_benefit_system,
        reform = None,
        variables = variables,
        )

    if baseline_tax_benefit_system is not None:
        df_baseline = calculate(
            period = period,
            scenarios_kwargs = scenarios_kwargs,
            tax_benefit_system = baseline_tax_benefit_system,
            reform = None,
            variables = variables,
            )
        if difference:
            df = df - df_baseline
        else:
            df = df.merge(df_baseline, on = input_variable, suffixes = ['', '_baseline'])

    return df



def plot_tax_rates(
        baseline_tax_benefit_system = None,
        tax_benefit_system = None,
        period = None,
        denominator = 'salaire_de_base',
        numerator = 'revenu_disponible',
        tax_numerator = False,
        rates = 'marginal',
        round_unit = 1, # need to have a low unit for accurate results.
        graph_options = {'nb_smic_max': 7
                         },
        test_case_options = {'nb_enfants' : 3, 'enfants_age' : [5, 6, 7]},
        ):
    """
    Plot the graphs comparing the tax rates for a given test-case scenario, before and after a reform.

    :param denominator: Variable used as the denominator in the marginal tax rate computation.
    :param numerator: Variable used as the numerato in the marginal tax rate computation.
    :param rates: Compute 'marginal' tax rates or 'average' tax rates.
    :param tax_numerator: If True, the marginal tax rate formula changes (its the same as when the numerator is an income but minus 1 so that MRT is between 0 and 100%)
    """

    assert tax_benefit_system is not None

    if graph_options is None:
        graph_options = {}
    if test_case_options is None:
        test_case_options = {}
    default_title = 'Variying : {}, Target : {}'.format(denominator, numerator)

    if baseline_tax_benefit_system:
        baseline_df = create_test_case_dataframe(
            graph_options = graph_options,
            tax_benefit_system = baseline_tax_benefit_system,
            test_case_options = test_case_options,
            variables = [denominator, numerator],
            input_variable = denominator,
            period = period,
            )
    reform_df = create_test_case_dataframe(
        graph_options = graph_options,
        tax_benefit_system = tax_benefit_system,
        test_case_options = test_case_options,
        variables = [denominator, numerator],
        input_variable = denominator,
        period = period,
        )

    # Compute the denominator as a share of another variable (SMIC brut, SMIC net..)

    x_axis_denominator = graph_options.get('x_axis_denominator', 'smic_brut')
    if x_axis_denominator == 'smic_net':
        denominator_denominator = smic_annuel_net_by_year[int(period)]
    if x_axis_denominator == 'smic_brut':
        denominator_denominator = smic_annuel_brut_by_year[int(period)]

    if denominator_denominator is not None:
        if baseline_tax_benefit_system:
            baseline_df[denominator + '_en_part_de_{}'.format(x_axis_denominator)] = baseline_df[denominator] / denominator_denominator
            baseline_df[numerator + '_en_part_de_{}'.format(x_axis_denominator)] = baseline_df[numerator] / denominator_denominator
        reform_df[denominator + '_en_part_de_{}'.format(x_axis_denominator)] = reform_df[denominator] / denominator_denominator
        reform_df[numerator + '_en_part_de_{}'.format(x_axis_denominator)] = reform_df[numerator] / denominator_denominator
        if denominator in base.label_by_variable.keys():
            base.label_by_variable[
                denominator + '_en_part_de_{}'.format(x_axis_denominator)
                ] = base.label_by_variable[denominator] + ' en part de ' + base.label_by_denominator[x_axis_denominator]
        denominator = denominator + '_en_part_de_{}'.format(x_axis_denominator)
        numerator = numerator + '_en_part_de_{}'.format(x_axis_denominator)

    # Compute tax rates

    rates_df = pd.DataFrame()
    if rates == 'marginal':
        rate_function = marginal_rate
        if tax_numerator:
            rate_function = marginal_rate_bis
        rates_df[denominator] = reform_df[denominator].values[:-1]
    if rates == 'average':
        rate_function = average_rate
        if tax_numerator:
            rate_function = average_rate_bis
        rates_df[denominator] = reform_df[denominator]

    rates_df['reform_{}_tax_rate'.format(rates)] = rate_function(
        reform_df[numerator].values,
        reform_df[denominator].values,
        trim = [-0.99, 0.99],
        ).round(round_unit)
    if baseline_tax_benefit_system:
        rates_df['baseline_{}_tax_rate'.format(rates)] = rate_function(
            baseline_df[numerator].values,
            baseline_df[denominator].values,
            trim = [-0.99, 0.99],
            ).round(round_unit)

    # Produce a graph

    rates_df.fillna(method = 'bfill', inplace = True) # to make graph looks nicer
    ax = rates_df.rename(columns = base.label_by_variable).plot(
        x = _(denominator),
        grid = True, color = base.color_by_variable, title = graph_options.get('title', default_title)
        )
    set_color(ax)
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.legend(frameon = True)
    rates_figure = ax.get_figure()

    return rates_figure, rates_df


# Helpers

def _(variable):
    if variable in base.label_by_variable.keys():
        return base.label_by_variable[variable]
    else:
        return variable

def set_color(ax, color_by_variable = base.color_by_variable):
    handles, labels = ax.get_legend_handles_labels()
    for label, handle in dict(zip(labels, handles)).items():
        if label in list(color_by_variable.keys()):
            handle.set_color(color_by_variable[label])
        ax.legend()


def average_rate_bis(target = None, varying = None, trim = None):
    '''
    Computes the average rate of a targeted net income, according to the varying gross income.

    :param target: Targeted net income, numerator
    :param varying: Varying gross income, denominator
    :param trim: Lower and upper bound of average rate to return
    '''
    average_rate = - target / varying
    if trim is not None:
        average_rate = numpy.where(average_rate <= max(trim), average_rate, numpy.nan)
        average_rate = numpy.where(average_rate >= min(trim), average_rate, numpy.nan)

    return average_rate


def marginal_rate_bis(target = None, varying = None, trim = None):
    # target: numerator, varying: denominator
    marginal_rate = - (target[:-1] - target[1:]) / (varying[:-1] - varying[1:])
    if trim is not None:
        marginal_rate = numpy.where(marginal_rate <= max(trim), marginal_rate, numpy.nan)
        marginal_rate = numpy.where(marginal_rate >= min(trim), marginal_rate, numpy.nan)

    return marginal_rate



#############

#CASES

#Case 1: Individu seul sans enfant

def plot_tax_rates_1(
        baseline_tax_benefit_system = None,
        tax_benefit_system = tax_benefit_system,
        period = period,
        denominator = 'salaire_de_base',
        numerator = 'revenu_disponible',
        tax_numerator = False,
        rates = 'marginal',
        round_unit = 1,# need to have a low unit for accurate results.
        graph_options = {
            'nb_smic_max': 3
            },
        test_case_options = {
            "couple": False,
            "nb_enfants": 0,
            "recourant_rsa":True,
            },
        ):
    """
    Plot the graphs comparing the tax rates for a given test-case scenario, before and after a reform.

    :param denominator: Variable used as the denominator in the marginal tax rate computation.
    :param numerator: Variable used as the numerato in the marginal tax rate computation.
    :param rates: Compute 'marginal' tax rates or 'average' tax rates.
    :param tax_numerator: If True, the marginal tax rate formula changes (its the same as when the numerator is an income but minus 1 so that MRT is between 0 and 100%)
    """

    assert tax_benefit_system is not None

    if graph_options is None:
        graph_options = {}
    if test_case_options is None:
        test_case_options = {}
    default_title = 'Cas 1'

    if baseline_tax_benefit_system:
        baseline_df = create_test_case_dataframe(
            graph_options = graph_options,
            tax_benefit_system = baseline_tax_benefit_system,
            test_case_options = test_case_options,
            variables = [denominator, numerator],
            input_variable = denominator,
            period = period,
            )
    reform_df = create_test_case_dataframe(
        graph_options = graph_options,
        tax_benefit_system = tax_benefit_system,
        test_case_options = test_case_options,
        variables = [denominator, numerator],
        input_variable = denominator,
        period = period,
        )



    # Compute tax rates

    rates_df = pd.DataFrame()
    if rates == 'marginal':
        rate_function = marginal_rate
        if tax_numerator:
            rate_function = marginal_rate_bis
        rates_df[denominator] = reform_df[denominator].values[:-1]
    if rates == 'average':
        rate_function = average_rate
        if tax_numerator:
            rate_function = average_rate_bis
        rates_df[denominator] = reform_df[denominator]

    rates_df['reform_{}_tax_rate'.format(rates)] = rate_function(
        reform_df[numerator].values,
        reform_df[denominator].values,
        trim = [-0.99, 0.99],
        ).round(round_unit)
    if baseline_tax_benefit_system:
        rates_df['baseline_{}_tax_rate'.format(rates)] = rate_function(
            baseline_df[numerator].values,
            baseline_df[denominator].values,
            trim = [-0.99, 0.99],
            ).round(round_unit)
    rates_df.to_csv(data_output_path+'cas1.csv', sep = ';')

    # Produce a graph

    rates_df.fillna(method = 'bfill', inplace = True) # to make graph looks nicer
    ax = rates_df.rename(columns = base.label_by_variable).plot(
        x = _(denominator),
#        grid = True, color = base.color_by_variable, title = graph_options.get('title', default_title)
        )
    set_color(ax)
    ax.set_xlabel("Gross wage")
    ax.set_ylabel("Effective Marginal Tax Rate")
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.legend(['Taux marginal de taxation'],loc='lower left',frameon = True).remove()
    rates_figure = ax.get_figure()
    plt.axvline(x=17880, color='black', ls=':')
    plt.axvline(x=21250, color='black', ls=':')
    plt.axvline(x=31200, color='black', ls=':')
    plt.axvline(x=46800, color='black', ls=':')
    ax.text(17880, 1.05, "P25", ha='center')
    ax.text(21250, 1.05, "P50", ha='center')
    ax.text(31200, 1.05, "P75", ha='center')
    ax.text(46800, 1.05, "P90", ha='center')
    rates_figure.savefig(graphs_path+"cas1.png")


    return rates_figure, rates_df

#rates_figure.savefig(graphs_path+"cas_1.png")

plot_tax_rates_1()


#Case 2: Individu avec 1 enfant

def plot_tax_rates_2(
        baseline_tax_benefit_system = None,
        tax_benefit_system = tax_benefit_system,
        period = period,
        denominator = 'salaire_de_base',
        numerator = 'revenu_disponible',
        tax_numerator = False,
        rates = 'marginal',
        round_unit = 1, # need to have a low unit for accurate results.
        graph_options = {
            'nb_smic_max': 3
            },
        test_case_options = {
            "couple": False,
            "nb_enfants": 1,
            "recourant_rsa":True,
            'enfants_age' : [5],
            },
        ):
    """
    Plot the graphs comparing the tax rates for a given test-case scenario, before and after a reform.

    :param denominator: Variable used as the denominator in the marginal tax rate computation.
    :param numerator: Variable used as the numerato in the marginal tax rate computation.
    :param rates: Compute 'marginal' tax rates or 'average' tax rates.
    :param tax_numerator: If True, the marginal tax rate formula changes (its the same as when the numerator is an income but minus 1 so that MRT is between 0 and 100%)
    """

    assert tax_benefit_system is not None

    if graph_options is None:
        graph_options = {}
    if test_case_options is None:
        test_case_options = {}
    default_title = 'Cas 2'

    if baseline_tax_benefit_system:
        baseline_df = create_test_case_dataframe(
            graph_options = graph_options,
            tax_benefit_system = baseline_tax_benefit_system,
            test_case_options = test_case_options,
            variables = [denominator, numerator],
            input_variable = denominator,
            period = period,
            )
    reform_df = create_test_case_dataframe(
        graph_options = graph_options,
        tax_benefit_system = tax_benefit_system,
        test_case_options = test_case_options,
        variables = [denominator, numerator],
        input_variable = denominator,
        period = period,
        )



    # Compute tax rates

    rates_df = pd.DataFrame()
    if rates == 'marginal':
        rate_function = marginal_rate
        if tax_numerator:
            rate_function = marginal_rate_bis
        rates_df[denominator] = reform_df[denominator].values[:-1]
    if rates == 'average':
        rate_function = average_rate
        if tax_numerator:
            rate_function = average_rate_bis
        rates_df[denominator] = reform_df[denominator]

    rates_df['reform_{}_tax_rate'.format(rates)] = rate_function(
        reform_df[numerator].values,
        reform_df[denominator].values,
        trim = [-0.99, 0.99],
        ).round(round_unit)
    if baseline_tax_benefit_system:
        rates_df['baseline_{}_tax_rate'.format(rates)] = rate_function(
            baseline_df[numerator].values,
            baseline_df[denominator].values,
            trim = [-0.99, 0.99],
            ).round(round_unit)
    rates_df.to_csv(data_output_path+'cas2.csv', sep = ';')

    # Produce a graph

    rates_df.fillna(method = 'bfill', inplace = True) # to make graph looks nicer
    ax = rates_df.rename(columns = base.label_by_variable).plot(
        x = _(denominator),
#        grid = True, color = base.color_by_variable, title = graph_options.get('title', default_title)
        )
    set_color(ax)
    ax.set_xlabel("Gross wage")
    ax.set_ylabel("Effective Marginal Tax Rate")
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.legend(['Taux marginal de taxation'],loc='lower left',frameon = False).remove()
    rates_figure = ax.get_figure()
    plt.axvline(x=17880, color='black', ls=':')
    plt.axvline(x=21250, color='black', ls=':')
    plt.axvline(x=31200, color='black', ls=':')
    plt.axvline(x=46800, color='black', ls=':')
    ax.text(17880, 0.85, "P25", ha='center')
    ax.text(21250, 0.85, "P50", ha='center')
    ax.text(31200, 0.85, "P75", ha='center')
    ax.text(46800, 0.85, "P90", ha='center')
    rates_figure.savefig(graphs_path+"cas2.png")


    return rates_figure, rates_df

plot_tax_rates_2()


#Case 3: Individu avec 2 enfants

def plot_tax_rates_3(
        baseline_tax_benefit_system = None,
        tax_benefit_system = tax_benefit_system,
        period = period,
        denominator = 'salaire_de_base',
        numerator = 'revenu_disponible',
        tax_numerator = False,
        rates = 'marginal',
        round_unit = 1, # need to have a low unit for accurate results.
        graph_options = {
            'nb_smic_max': 3
            },
        test_case_options = {
            "couple": False,
            "nb_enfants": 2,
            "recourant_rsa":True,
            'enfants_age' : [5,6],
            },
        ):
    """
    Plot the graphs comparing the tax rates for a given test-case scenario, before and after a reform.

    :param denominator: Variable used as the denominator in the marginal tax rate computation.
    :param numerator: Variable used as the numerato in the marginal tax rate computation.
    :param rates: Compute 'marginal' tax rates or 'average' tax rates.
    :param tax_numerator: If True, the marginal tax rate formula changes (its the same as when the numerator is an income but minus 1 so that MRT is between 0 and 100%)
    """

    assert tax_benefit_system is not None

    if graph_options is None:
        graph_options = {}
    if test_case_options is None:
        test_case_options = {}
    default_title = 'Cas 3'

    if baseline_tax_benefit_system:
        baseline_df = create_test_case_dataframe(
            graph_options = graph_options,
            tax_benefit_system = baseline_tax_benefit_system,
            test_case_options = test_case_options,
            variables = [denominator, numerator],
            input_variable = denominator,
            period = period,
            )
    reform_df = create_test_case_dataframe(
        graph_options = graph_options,
        tax_benefit_system = tax_benefit_system,
        test_case_options = test_case_options,
        variables = [denominator, numerator],
        input_variable = denominator,
        period = period,
        )


    # Compute tax rates

    rates_df = pd.DataFrame()
    if rates == 'marginal':
        rate_function = marginal_rate
        if tax_numerator:
            rate_function = marginal_rate_bis
        rates_df[denominator] = reform_df[denominator].values[:-1]
    if rates == 'average':
        rate_function = average_rate
        if tax_numerator:
            rate_function = average_rate_bis
        rates_df[denominator] = reform_df[denominator]

    rates_df['reform_{}_tax_rate'.format(rates)] = rate_function(
        reform_df[numerator].values,
        reform_df[denominator].values,
        trim = [-0.99, 0.99],
        ).round(round_unit)
    if baseline_tax_benefit_system:
        rates_df['baseline_{}_tax_rate'.format(rates)] = rate_function(
            baseline_df[numerator].values,
            baseline_df[denominator].values,
            trim = [-0.99, 0.99],
            ).round(round_unit)
    rates_df.to_csv(data_output_path+'cas3.csv', sep = ';')

    # Produce a graph

    rates_df.fillna(method = 'bfill', inplace = True) # to make graph looks nicer
    ax = rates_df.rename(columns = base.label_by_variable).plot(
        x = _(denominator),
#        grid = True, color = base.color_by_variable, title = graph_options.get('title', default_title)
        )
    set_color(ax)
    ax.set_xlabel("Gross wage")
    ax.set_ylabel("Effective Marginal Tax Rate")
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.legend(['Taux marginal de taxation'],loc='lower left',frameon = True).remove()
    rates_figure = ax.get_figure()
    plt.axvline(x=17880, color='black', ls=':')
    plt.axvline(x=21250, color='black', ls=':')
    plt.axvline(x=31200, color='black', ls=':')
    plt.axvline(x=46800, color='black', ls=':')
    ax.text(17880, 0.95, "P25", ha='center')
    ax.text(21250, 0.95, "P50", ha='center')
    ax.text(31200, 0.95, "P75", ha='center')
    ax.text(46800, 0.95, "P90", ha='center')
    rates_figure.savefig(graphs_path+"cas3.png")


    return rates_figure, rates_df

plot_tax_rates_3()

#Case 4: Individu avec 3 enfants

def plot_tax_rates_4(
        baseline_tax_benefit_system = None,
        tax_benefit_system = tax_benefit_system,
        period = period,
        denominator = 'salaire_de_base',
        numerator = 'revenu_disponible',
        tax_numerator = False,
        rates = 'marginal',
        round_unit = 1, # need to have a low unit for accurate results.
        graph_options = {
            'nb_smic_max': 3
            },
        test_case_options = {
            "couple": False,
            "nb_enfants": 3,
            "recourant_rsa":True,
            'enfants_age' : [5,6,7],
            },
        ):
    """
    Plot the graphs comparing the tax rates for a given test-case scenario, before and after a reform.

    :param denominator: Variable used as the denominator in the marginal tax rate computation.
    :param numerator: Variable used as the numerato in the marginal tax rate computation.
    :param rates: Compute 'marginal' tax rates or 'average' tax rates.
    :param tax_numerator: If True, the marginal tax rate formula changes (its the same as when the numerator is an income but minus 1 so that MRT is between 0 and 100%)
    """

    assert tax_benefit_system is not None

    if graph_options is None:
        graph_options = {}
    if test_case_options is None:
        test_case_options = {}
    default_title = 'Cas 4'

    if baseline_tax_benefit_system:
        baseline_df = create_test_case_dataframe(
            graph_options = graph_options,
            tax_benefit_system = baseline_tax_benefit_system,
            test_case_options = test_case_options,
            variables = [denominator, numerator],
            input_variable = denominator,
            period = period,
            )
    reform_df = create_test_case_dataframe(
        graph_options = graph_options,
        tax_benefit_system = tax_benefit_system,
        test_case_options = test_case_options,
        variables = [denominator, numerator],
        input_variable = denominator,
        period = period,
        )



    # Compute tax rates

    rates_df = pd.DataFrame()
    if rates == 'marginal':
        rate_function = marginal_rate
        if tax_numerator:
            rate_function = marginal_rate_bis
        rates_df[denominator] = reform_df[denominator].values[:-1]
    if rates == 'average':
        rate_function = average_rate
        if tax_numerator:
            rate_function = average_rate_bis
        rates_df[denominator] = reform_df[denominator]

    rates_df['reform_{}_tax_rate'.format(rates)] = rate_function(
        reform_df[numerator].values,
        reform_df[denominator].values,
        trim = [-0.99, 0.99],
        ).round(round_unit)
    if baseline_tax_benefit_system:
        rates_df['baseline_{}_tax_rate'.format(rates)] = rate_function(
            baseline_df[numerator].values,
            baseline_df[denominator].values,
            trim = [-0.99, 0.99],
            ).round(round_unit)
    rates_df.to_csv(data_output_path+'cas4.csv', sep = ';')

    # Produce a graph

    rates_df.fillna(method = 'bfill', inplace = True) # to make graph looks nicer
    ax = rates_df.rename(columns = base.label_by_variable).plot(
        x = _(denominator),
#        grid = True, color = base.color_by_variable, title = graph_options.get('title', default_title)
        )
    set_color(ax)
    ax.set_xlabel("Gross wage")
    ax.set_ylabel("Effective Marginal Tax Rate")
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.legend(['Taux marginal de taxation'],loc='lower left',frameon = True).remove()
    rates_figure = ax.get_figure()
    plt.axvline(x=17880, color='black', ls=':')
    plt.axvline(x=21250, color='black', ls=':')
    plt.axvline(x=31200, color='black', ls=':')
    plt.axvline(x=46800, color='black', ls=':')
    ax.text(17880, 0.85, "P25", ha='center')
    ax.text(21250, 0.85, "P50", ha='center')
    ax.text(31200, 0.85, "P75", ha='center')
    ax.text(46800, 0.85, "P90", ha='center')
    rates_figure.savefig(graphs_path+"cas4.png")


    return rates_figure, rates_df

plot_tax_rates_4()

#Cas 5 : Couple sans enfant

def plot_tax_rates_5(
        baseline_tax_benefit_system = None,
        tax_benefit_system = tax_benefit_system,
        period = period,
        denominator = 'salaire_de_base',
        numerator = 'revenu_disponible',
        tax_numerator = False,
        rates = 'marginal',
        round_unit = 1, # need to have a low unit for accurate results.
        graph_options = {
            'nb_smic_max': 3
            },
        test_case_options = {
            "couple": True,
            "nb_enfants": 0,
            "recourant_rsa":True,
            },
        ):
    """
    Plot the graphs comparing the tax rates for a given test-case scenario, before and after a reform.

    :param denominator: Variable used as the denominator in the marginal tax rate computation.
    :param numerator: Variable used as the numerato in the marginal tax rate computation.
    :param rates: Compute 'marginal' tax rates or 'average' tax rates.
    :param tax_numerator: If True, the marginal tax rate formula changes (its the same as when the numerator is an income but minus 1 so that MRT is between 0 and 100%)
    """

    assert tax_benefit_system is not None

    if graph_options is None:
        graph_options = {}
    if test_case_options is None:
        test_case_options = {}
    default_title = 'Cas 5'

    if baseline_tax_benefit_system:
        baseline_df = create_test_case_dataframe(
            graph_options = graph_options,
            tax_benefit_system = baseline_tax_benefit_system,
            test_case_options = test_case_options,
            variables = [denominator, numerator],
            input_variable = denominator,
            period = period,
            )
    reform_df = create_test_case_dataframe(
        graph_options = graph_options,
        tax_benefit_system = tax_benefit_system,
        test_case_options = test_case_options,
        variables = [denominator, numerator],
        input_variable = denominator,
        period = period,
        )



    # Compute tax rates

    rates_df = pd.DataFrame()
    if rates == 'marginal':
        rate_function = marginal_rate
        if tax_numerator:
            rate_function = marginal_rate_bis
        rates_df[denominator] = reform_df[denominator].values[:-1]
    if rates == 'average':
        rate_function = average_rate
        if tax_numerator:
            rate_function = average_rate_bis
        rates_df[denominator] = reform_df[denominator]

    rates_df['reform_{}_tax_rate'.format(rates)] = rate_function(
        reform_df[numerator].values,
        reform_df[denominator].values,
        trim = [-0.99, 0.99],
        ).round(round_unit)
    if baseline_tax_benefit_system:
        rates_df['baseline_{}_tax_rate'.format(rates)] = rate_function(
            baseline_df[numerator].values,
            baseline_df[denominator].values,
            trim = [-0.99, 0.99],
            ).round(round_unit)
    rates_df.to_csv(data_output_path+'cas5.csv', sep = ';')

    # Produce a graph

    rates_df.fillna(method = 'bfill', inplace = True) # to make graph looks nicer
    ax = rates_df.rename(columns = base.label_by_variable).plot(
        x = _(denominator),
#        grid = True, color = base.color_by_variable, title = graph_options.get('title', default_title)
        )
    set_color(ax)
    ax.set_xlabel("Gross wage")
    ax.set_ylabel("Effective Marginal Tax Rate")
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.legend(['Taux marginal de taxation'],loc='lower left',frameon = True).remove()
    rates_figure = ax.get_figure()
    plt.axvline(x=17880, color='black', ls=':')
    plt.axvline(x=21250, color='black', ls=':')
    plt.axvline(x=31200, color='black', ls=':')
    plt.axvline(x=46800, color='black', ls=':')
    ax.text(17880, 0.55, "P25", ha='center')
    ax.text(21250, 0.55, "P50", ha='center')
    ax.text(31200, 0.55, "P75", ha='center')
    ax.text(46800, 0.55, "P90", ha='center')
    rates_figure.savefig(graphs_path+"cas5.png")


    return rates_figure, rates_df

plot_tax_rates_5()

#Cas 6 : Couple avec 1 enfant

def plot_tax_rates_6(
        baseline_tax_benefit_system = None,
        tax_benefit_system = tax_benefit_system,
        period = period,
        denominator = 'salaire_de_base',
        numerator = 'revenu_disponible',
        tax_numerator = False,
        rates = 'marginal',
        round_unit = 1, # need to have a low unit for accurate results.
        graph_options = {
            'nb_smic_max': 3
            },
        test_case_options = {
            "couple": True,
            "nb_enfants": 1,
            "recourant_rsa":True,
            'enfants_age' : [5],
            },
        ):
    """
    Plot the graphs comparing the tax rates for a given test-case scenario, before and after a reform.

    :param denominator: Variable used as the denominator in the marginal tax rate computation.
    :param numerator: Variable used as the numerato in the marginal tax rate computation.
    :param rates: Compute 'marginal' tax rates or 'average' tax rates.
    :param tax_numerator: If True, the marginal tax rate formula changes (its the same as when the numerator is an income but minus 1 so that MRT is between 0 and 100%)
    """

    assert tax_benefit_system is not None

    if graph_options is None:
        graph_options = {}
    if test_case_options is None:
        test_case_options = {}
    default_title = 'Cas 6'

    if baseline_tax_benefit_system:
        baseline_df = create_test_case_dataframe(
            graph_options = graph_options,
            tax_benefit_system = baseline_tax_benefit_system,
            test_case_options = test_case_options,
            variables = [denominator, numerator],
            input_variable = denominator,
            period = period,
            )
    reform_df = create_test_case_dataframe(
        graph_options = graph_options,
        tax_benefit_system = tax_benefit_system,
        test_case_options = test_case_options,
        variables = [denominator, numerator],
        input_variable = denominator,
        period = period,
        )



    # Compute tax rates

    rates_df = pd.DataFrame()
    if rates == 'marginal':
        rate_function = marginal_rate
        if tax_numerator:
            rate_function = marginal_rate_bis
        rates_df[denominator] = reform_df[denominator].values[:-1]
    if rates == 'average':
        rate_function = average_rate
        if tax_numerator:
            rate_function = average_rate_bis
        rates_df[denominator] = reform_df[denominator]

    rates_df['reform_{}_tax_rate'.format(rates)] = rate_function(
        reform_df[numerator].values,
        reform_df[denominator].values,
        trim = [-0.99, 0.99],
        ).round(round_unit)
    if baseline_tax_benefit_system:
        rates_df['baseline_{}_tax_rate'.format(rates)] = rate_function(
            baseline_df[numerator].values,
            baseline_df[denominator].values,
            trim = [-0.99, 0.99],
            ).round(round_unit)
    rates_df.to_csv(data_output_path+'cas6.csv', sep = ';')

    # Produce a graph

    rates_df.fillna(method = 'bfill', inplace = True) # to make graph looks nicer
    ax = rates_df.rename(columns = base.label_by_variable).plot(
        x = _(denominator),
#       grid = True, color = base.color_by_variable, title = graph_options.get('title', default_title)
        )
    set_color(ax)
    ax.set_xlabel("Gross wage")
    ax.set_ylabel("Effective Marginal Tax Rate")
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.legend(['Taux marginal de taxation'],loc='lower left',frameon = True).remove()
    rates_figure = ax.get_figure()
    plt.axvline(x=17880, color='black', ls=':')
    plt.axvline(x=21250, color='black', ls=':')
    plt.axvline(x=31200, color='black', ls=':')
    plt.axvline(x=46800, color='black', ls=':')
    ax.text(17880, 1.08, "P25", ha='center')
    ax.text(21250, 1.08, "P50", ha='center')
    ax.text(31200, 1.08, "P75", ha='center')
    ax.text(46800, 1.08, "P90", ha='center')
    rates_figure.savefig(graphs_path+"cas6.png")


    return rates_figure, rates_df

plot_tax_rates_6()

#Cas 7 : couple avec 2 enfants

def plot_tax_rates_7(
        baseline_tax_benefit_system = None,
        tax_benefit_system = tax_benefit_system,
        period = period,
        denominator = 'salaire_de_base',
        numerator = 'revenu_disponible',
        tax_numerator = False,
        rates = 'marginal',
        round_unit = 1, # need to have a low unit for accurate results.
        graph_options = {
            'nb_smic_max': 3
            },
        test_case_options = {
            "couple": True,
            "nb_enfants": 2,
            "recourant_rsa":True,
            'enfants_age' : [5,6],
            },
        ):
    """
    Plot the graphs comparing the tax rates for a given test-case scenario, before and after a reform.

    :param denominator: Variable used as the denominator in the marginal tax rate computation.
    :param numerator: Variable used as the numerato in the marginal tax rate computation.
    :param rates: Compute 'marginal' tax rates or 'average' tax rates.
    :param tax_numerator: If True, the marginal tax rate formula changes (its the same as when the numerator is an income but minus 1 so that MRT is between 0 and 100%)
    """

    assert tax_benefit_system is not None

    if graph_options is None:
        graph_options = {}
    if test_case_options is None:
        test_case_options = {}
    default_title = 'Cas 7'

    if baseline_tax_benefit_system:
        baseline_df = create_test_case_dataframe(
            graph_options = graph_options,
            tax_benefit_system = baseline_tax_benefit_system,
            test_case_options = test_case_options,
            variables = [denominator, numerator],
            input_variable = denominator,
            period = period,
            )
    reform_df = create_test_case_dataframe(
        graph_options = graph_options,
        tax_benefit_system = tax_benefit_system,
        test_case_options = test_case_options,
        variables = [denominator, numerator],
        input_variable = denominator,
        period = period,
        )



    # Compute tax rates

    rates_df = pd.DataFrame()
    if rates == 'marginal':
        rate_function = marginal_rate
        if tax_numerator:
            rate_function = marginal_rate_bis
        rates_df[denominator] = reform_df[denominator].values[:-1]
    if rates == 'average':
        rate_function = average_rate
        if tax_numerator:
            rate_function = average_rate_bis
        rates_df[denominator] = reform_df[denominator]

    rates_df['reform_{}_tax_rate'.format(rates)] = rate_function(
        reform_df[numerator].values,
        reform_df[denominator].values,
        trim = [-0.99, 0.99],
        ).round(round_unit)
    if baseline_tax_benefit_system:
        rates_df['baseline_{}_tax_rate'.format(rates)] = rate_function(
            baseline_df[numerator].values,
            baseline_df[denominator].values,
            trim = [-0.99, 0.99],
            ).round(round_unit)
    rates_df.to_csv(data_output_path+'cas7.csv', sep = ';')

    # Produce a graph

    rates_df.fillna(method = 'bfill', inplace = True) # to make graph looks nicer
    ax = rates_df.rename(columns = base.label_by_variable).plot(
        x = _(denominator),
#        grid = True, color = base.color_by_variable, title = graph_options.get('title', default_title)
        )
    set_color(ax)
    ax.set_xlabel("Gross wage")
    ax.set_ylabel("Effective Marginal Tax Rate")
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.legend(['Taux marginal de taxation'],loc='lower left',frameon = True).remove()
    rates_figure = ax.get_figure()
    plt.axvline(x=17880, color='black', ls=':')
    plt.axvline(x=21250, color='black', ls=':')
    plt.axvline(x=31200, color='black', ls=':')
    plt.axvline(x=46800, color='black', ls=':')
    ax.text(17880, 0.55, "P25", ha='center')
    ax.text(21250, 0.55, "P50", ha='center')
    ax.text(31200, 0.55, "P75", ha='center')
    ax.text(46800, 0.55, "P90", ha='center')
    rates_figure.savefig(graphs_path+"cas7.png")


    return rates_figure, rates_df

plot_tax_rates_7()

#Cas 8 : Couple avec 3 enfants

def plot_tax_rates_8(
        baseline_tax_benefit_system = None,
        tax_benefit_system = tax_benefit_system,
        period = period,
        denominator = 'salaire_de_base',
        numerator = 'revenu_disponible',
        tax_numerator = False,
        rates = 'marginal',
        round_unit = 1, # need to have a low unit for accurate results.
        graph_options = {
            'nb_smic_max': 3
            },
        test_case_options = {
            "couple": True,
            "nb_enfants": 3,
            "recourant_rsa":True,
            'enfants_age' : [5,6,7],
            },
        ):
    """
    Plot the graphs comparing the tax rates for a given test-case scenario, before and after a reform.

    :param denominator: Variable used as the denominator in the marginal tax rate computation.
    :param numerator: Variable used as the numerato in the marginal tax rate computation.
    :param rates: Compute 'marginal' tax rates or 'average' tax rates.
    :param tax_numerator: If True, the marginal tax rate formula changes (its the same as when the numerator is an income but minus 1 so that MRT is between 0 and 100%)
    """

    assert tax_benefit_system is not None

    if graph_options is None:
        graph_options = {}
    if test_case_options is None:
        test_case_options = {}
    default_title = 'Cas 8'

    if baseline_tax_benefit_system:
        baseline_df = create_test_case_dataframe(
            graph_options = graph_options,
            tax_benefit_system = baseline_tax_benefit_system,
            test_case_options = test_case_options,
            variables = [denominator, numerator],
            input_variable = denominator,
            period = period,
            )
    reform_df = create_test_case_dataframe(
        graph_options = graph_options,
        tax_benefit_system = tax_benefit_system,
        test_case_options = test_case_options,
        variables = [denominator, numerator],
        input_variable = denominator,
        period = period,
        )



    # Compute tax rates

    rates_df = pd.DataFrame()
    if rates == 'marginal':
        rate_function = marginal_rate
        if tax_numerator:
            rate_function = marginal_rate_bis
        rates_df[denominator] = reform_df[denominator].values[:-1]
    if rates == 'average':
        rate_function = average_rate
        if tax_numerator:
            rate_function = average_rate_bis
        rates_df[denominator] = reform_df[denominator]

    rates_df['reform_{}_tax_rate'.format(rates)] = rate_function(
        reform_df[numerator].values,
        reform_df[denominator].values,
        trim = [-0.99, 0.99],
        ).round(round_unit)
    if baseline_tax_benefit_system:
        rates_df['baseline_{}_tax_rate'.format(rates)] = rate_function(
            baseline_df[numerator].values,
            baseline_df[denominator].values,
            trim = [-0.99, 0.99],
            ).round(round_unit)
    rates_df.to_csv(data_output_path+'cas8.csv', sep = ';')

    # Produce a graph

    rates_df.fillna(method = 'bfill', inplace = True) # to make graph looks nicer
    ax = rates_df.rename(columns = base.label_by_variable).plot(
        x = _(denominator),
#        grid = True, color = base.color_by_variable, title = graph_options.get('title', default_title)
        )
    set_color(ax)
    ax.set_xlabel("Gross wage")
    ax.set_ylabel("Effective Marginal Tax Rate")
    ax.yaxis.set_major_formatter(to_percent_round_formatter)
    ax.legend(['Taux marginal de taxation'],loc='lower left',frameon = True).remove()
    rates_figure = ax.get_figure()
    plt.axvline(x=17880, color='black', ls=':')
    plt.axvline(x=21250, color='black', ls=':')
    plt.axvline(x=31200, color='black', ls=':')
    plt.axvline(x=46800, color='black', ls=':')
    ax.text(17880, 1.07, "P25", ha='center')
    ax.text(21250, 1.07, "P50", ha='center')
    ax.text(31200, 1.07, "P75", ha='center')
    ax.text(46800, 1.07, "P90", ha='center')
    rates_figure.savefig(graphs_path+"cas8.png")



    return rates_figure, rates_df

plot_tax_rates_8()
