*********************************************
* Quick descriptive statistics on the final sample kept of the ERFS surveys	*
* Here we are working with the final database used to compute densities and test functions*
*In the final document I change manually variables names*
*********************************************

*if 	"`c(username)'" == "pablorodriguez" {
*	global path "/Users/pablorodriguez/codes_stage_crest"
*	
*}
*cd "${path}"

global path "/Users/pablorodriguez/Desktop/codes_stage_crest"
cd "${path}"

*global year 2014
global year 2015
*global year 2018

use using "${path}/EMTR_study_withdensities_${year}.dta", clear
est clear  // clear the est locals

estpost tabstat mean_ff_age revenu_disponible_ff salaire_de_base, c(stat) stat(mean sd min P25 P50 P75 max n)

esttab using "${path}/descriptive_stats_${year}.tex", replace ///
 cells("mean(fmt(%13.2fc)) sd(fmt(%13.2fc)) min p25 p50 p75 max count") nonumber ///
  nomtitle nonote noobs label booktabs f ///
  collabels ("Mean" "SD" "Min" "P25" "P50" "P75" "Max" "N")
