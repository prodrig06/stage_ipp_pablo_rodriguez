# stage_ipp_pablo_rodriguez

In this repository you can find the files necessary to reproduce the output in "An Application of Pareto Test Functions in France 2015 and 2019". All the files and instructions to do so are contained in the folder codes_stage_crest.

In order to use Taxipp microsimulation model it is necessary to be on the branch "bilal" on Open-Fisca France Data and on the branch "test_functions_application_france" on Taxipp. This might be however subject to change.

Should you encounter any problem reproducing the output, you can contact me at pablo.rodriguez@ens-lyon.fr
